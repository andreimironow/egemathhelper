package com.andreimironov.egemathhelper.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.andreimironov.egemathhelper.R;
import com.andreimironov.egemathhelper.data.DatabaseHandlerThread;
import com.andreimironov.egemathhelper.data.SharedPrefAdapter;
import com.andreimironov.egemathhelper.fragments.mainfragment.MainFragment;
import com.andreimironov.egemathhelper.fragments.rulesfragment.RulesFragment;
import com.andreimironov.egemathhelper.fragments.statfragment.StatFragment;
import com.andreimironov.egemathhelper.fragments.testfragment.TestFragment;
import com.andreimironov.egemathhelper.fragments.theoryfragment.TheoryFragment;
import com.andreimironov.egemathhelper.models.answers.Answer;
import com.andreimironov.egemathhelper.models.Statistic;
import com.andreimironov.egemathhelper.models.QuestionInfo;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.navigation.NavigationView.OnNavigationItemSelectedListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements
        OnNavigationItemSelectedListener,
        TestFragment.TestFragmentListener,
        DatabaseHandlerThread.DatabaseHandlerThreadListener {
    private static final int ACTION_BEFORE_START_NEW_TEST = 0;
    private static final int ACTION_BEFORE_CONTINUE_TEST = 1;
    private static final int ACTION_BEFORE_CHECK_TEST = 2;
    private static final int ACTION_START_NEW_TEST = 3;
    private static final int ACTION_CONTINUE_TEST = 4;
    private static final int ACTION_CHECK_TEST = 5;
    private static final int ACTION_SHOW_ANSWER_VALIDATION_DIALOG = 6;
    private static final int ACTION_GO_TO_STATISTICS = 7;

    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private AlertDialog mProgressDialog;
    private MenuItem mCurrentMenuItem;

    private SharedPrefAdapter mSharedPrefAdapter;
    private DatabaseHandlerThread mDatabaseHandlerThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSharedPrefAdapter = SharedPrefAdapter.getSharedPrefAdapter(getApplicationContext());

        mDrawerLayout = findViewById(R.id.drawer_layout);
        mNavigationView = findViewById(R.id.navigation_view);
        mNavigationView.setNavigationItemSelectedListener(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                toolbar,
                R.string.open_drawer_action,
                R.string.close_drawer_action);
        mDrawerLayout.addDrawerListener(actionBarDrawerToggle);
        mProgressDialog = new AlertDialog
                .Builder(this)
                .setView(R.layout.dialog_spinner)
                .setCancelable(false)
                .create();

        Handler responseHandler = new Handler();
        mDatabaseHandlerThread = new DatabaseHandlerThread(
                responseHandler,
                getApplicationContext(),
                this);
        mDatabaseHandlerThread.start();
        // call getLooper to prevent race condition
        mDatabaseHandlerThread.getLooper();

        Integer savedMenuItemId = mSharedPrefAdapter.getMenuItemId();
        int currentMenuItemId = savedMenuItemId == null
                        ? R.id.menu_nav_main
                        : savedMenuItemId == R.id.menu_nav_new_test
                                ? R.id.menu_nav_continue_test
                                : savedMenuItemId;
        setCurrentItem(currentMenuItemId);

        MobileAds.initialize(this, getResources().getString(R.string.app_id));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getSupportFragmentManager().getFragments().isEmpty()) {
            goToMenuItem(mCurrentMenuItem);
        }
    }

    @Override
    protected void onDestroy() {
        mDatabaseHandlerThread.quitSafely();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //to avoid android.os.TransactionTooLargeException for StatFragment when hiding an app
        outState.clear();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof MainFragment) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.dialog_exit_the_app)
                    .setPositiveButton(R.string.dialog_continue_test_yes_title, new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            MainActivity.this.finish();
                        }
                    })
                    .setNegativeButton(R.string.dialog_continue_test_no_title, new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create()
                    .show();
        } else {
            MenuItem mainMenuItem = mNavigationView.getMenu().findItem(R.id.menu_nav_main);
            setCurrentItem(mainMenuItem.getItemId());
            goToMenuItem(mainMenuItem);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        if (!menuItem.equals(mCurrentMenuItem)) {
            setCurrentItem(menuItem.getItemId());
            mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
                @Override
                public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

                }

                @Override
                public void onDrawerOpened(@NonNull View drawerView) {

                }

                @Override
                public void onDrawerClosed(@NonNull View drawerView) {
                    mDrawerLayout.removeDrawerListener(this);
                    goToMenuItem(menuItem);
                }

                @Override
                public void onDrawerStateChanged(int newState) {

                }
            });
        }
        mDrawerLayout.closeDrawer(GravityCompat.START);
        // if return value is true then menu item will be checked
        // we handle the state of item, so we return false
        return false;
    }

    private void goToMenuItem(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_nav_main:
                onMainItemSelected();
                break;
            case R.id.menu_nav_new_test:
                onNewTestItemSelected();
                break;
            case R.id.menu_nav_continue_test:
                onContinueTestItemSelected();
                break;
            case R.id.menu_nav_check_test:
                onCheckTestItemSelected();
                break;
            case R.id.menu_nav_rules:
                onRulesItemSelected();
                break;
            case R.id.menu_nav_theory:
                onTheoryItemSelected();
                break;
            case R.id.menu_nav_stat:
                onStatItemSelected();
                break;
            default:
                break;
        }
    }

    private void onMainItemSelected() {
        replaceFragment(MainFragment.class, null);
    }

    private void onNewTestItemSelected() {
        startRequest(
                DatabaseHandlerThread.REQUEST_GET_CURRENT_STATISTIC,
                new Object() {
                    int mAction = ACTION_BEFORE_START_NEW_TEST;
                });

    }

    private void onContinueTestItemSelected() {
        startRequest(
                DatabaseHandlerThread.REQUEST_GET_CURRENT_STATISTIC,
                new Object() {
                    int mAction = ACTION_BEFORE_CONTINUE_TEST;
                });
    }

    private void onCheckTestItemSelected() {
        startRequest(
                DatabaseHandlerThread.REQUEST_GET_CURRENT_STATISTIC,
                new Object() {
                    int mAction = ACTION_BEFORE_CHECK_TEST;
                });
    }

    private void onStatItemSelected() {
        setCurrentItem(R.id.menu_nav_stat);
        startRequest(
                DatabaseHandlerThread.REQUEST_GET_ALL_QUESTION_INFOS,
                new Object() {
                    int mAction = ACTION_GO_TO_STATISTICS;
                });
    }

    private void onRulesItemSelected() {
        setCurrentItem(R.id.menu_nav_rules);
        replaceFragment(RulesFragment.class, null);
    }

    private void onTheoryItemSelected() {
        setCurrentItem(R.id.menu_nav_theory);
        replaceFragment(TheoryFragment.class, null);
    }

    private void startRequest(int request, Object object) {
        mProgressDialog.show();
        mDatabaseHandlerThread.obtainRequest(request, object);
    }

    private void startNewTest() {
        setCurrentItem(R.id.menu_nav_new_test);
        startRequest(
                DatabaseHandlerThread.REQUEST_INSERT_STATISTIC_AND_GET_CURRENT_QUESTION_INFOS,
                new Object() {
                    int mAction = ACTION_START_NEW_TEST;
                });
    }

    private void checkTestAndStartNewTest() {
        setCurrentItem(R.id.menu_nav_new_test);
        startRequest(
                DatabaseHandlerThread.REQUEST_UPDATE_AND_INSERT_STATISTIC_AND_GET_CURRENT_QUESTION_INFOS,
                new Object() {
                    int mAction = ACTION_START_NEW_TEST;
                });
    }

    private void continueTest() {
        setCurrentItem(R.id.menu_nav_continue_test);
        startRequest(
                DatabaseHandlerThread.REQUEST_GET_CURRENT_QUESTION_INFOS,
                new Object() {
                    int mAction = ACTION_CONTINUE_TEST;
                });
    }

    private void checkTest() {
        setCurrentItem(R.id.menu_nav_check_test);
        startRequest(
                DatabaseHandlerThread.REQUEST_UPDATE_STATISTIC_AND_GET_ALL_QUESTION_INFOS,
                new Object() {
                    int mAction = ACTION_CHECK_TEST;
                });
    }

    private void setCurrentItem(int menuItemId) {
        MenuItem menuItem = mNavigationView.getMenu().findItem(menuItemId);
        if (mCurrentMenuItem != null) {
            mCurrentMenuItem.setChecked(false);
        }
        mCurrentMenuItem = menuItem;
        mCurrentMenuItem.setChecked(true);
        mSharedPrefAdapter.putMenuItemId(menuItemId);
    }

    @Override
    public void onAnswerReceived(
            long id,
            Map<Integer, Answer> newUserAnswers,
            boolean areNewUserAnswersValid
    ) {
        startRequest(
                DatabaseHandlerThread.REQUEST_UPDATE_STATISTIC_ENTRY,
                new Object() {
                    int mAction = ACTION_SHOW_ANSWER_VALIDATION_DIALOG;
                    long mId = id;
                    Map<Integer, Answer> mNewUserAnswers = newUserAnswers;
                    boolean mAreNewUserAnswersValid = areNewUserAnswersValid;
                });
    }

    @Override
    public void onFragmentLoaded() {
        mProgressDialog.dismiss();
    }

    @Override
    public void onResponseReceived(int request, int action, Object response) {
        switch (request) {
            case DatabaseHandlerThread.REQUEST_GET_CURRENT_STATISTIC:
                mProgressDialog.dismiss();
                Statistic statistic = (Statistic) response;
                onCurrentStatisticReceived(action, statistic);
                break;
            case DatabaseHandlerThread.REQUEST_GET_CURRENT_QUESTION_INFOS:
                replaceFragment(TestFragment.class, response);
                break;
            case DatabaseHandlerThread.REQUEST_INSERT_STATISTIC_AND_GET_CURRENT_QUESTION_INFOS:
                replaceFragment(TestFragment.class, response);
                break;
            case DatabaseHandlerThread.REQUEST_UPDATE_AND_INSERT_STATISTIC_AND_GET_CURRENT_QUESTION_INFOS:
                replaceFragment(TestFragment.class, response);
                break;
            case DatabaseHandlerThread.REQUEST_UPDATE_STATISTIC_AND_GET_ALL_QUESTION_INFOS:
                mProgressDialog.dismiss();
                replaceFragment(StatFragment.class, response);
                break;
            case DatabaseHandlerThread.REQUEST_UPDATE_STATISTIC_ENTRY:
                mProgressDialog.dismiss();
                boolean areNewUserAnswersValid = (boolean) response;
                if (areNewUserAnswersValid) {
                    new AlertDialog.Builder(MainActivity.this)
                            .setMessage(R.string.dialog_answer_is_confirmed_message)
                            .create()
                            .show();
                }
                else {
                    new AlertDialog.Builder(MainActivity.this)
                            .setMessage(R.string.dialog_answer_is_not_confirmed_message)
                            .create()
                            .show();
                }
                break;
            case DatabaseHandlerThread.REQUEST_GET_ALL_QUESTION_INFOS:
                mProgressDialog.dismiss();
                replaceFragment(StatFragment.class, response);
                break;
            default:
                break;
        }
    }

    private void onCurrentStatisticReceived(int action, Statistic statistic) {
        switch (action) {
            case ACTION_BEFORE_START_NEW_TEST:
                if (statistic == null) {
                    startNewTest();
                    return;
                }
                OnClickListener newTestDialogListener = new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_NEGATIVE:
                                continueTest();
                                break;
                            case DialogInterface.BUTTON_POSITIVE:
                                checkTestAndStartNewTest();
                                break;
                            default:
                                break;
                        }
                    }
                };
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage(R.string.dialog_new_test_message)
                        .setPositiveButton(R.string.dialog_new_test_yes_title, newTestDialogListener)
                        .setNegativeButton(R.string.dialog_new_test_no_title, newTestDialogListener)
                        .create()
                        .show();
                break;
            case ACTION_BEFORE_CONTINUE_TEST:
                if (statistic == null) {
                    OnClickListener continueTestDialogListener = new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_NEGATIVE:
                                    break;
                                case DialogInterface.BUTTON_POSITIVE:
                                    startNewTest();
                                    break;
                                default:
                                    break;
                            }
                        }
                    };
                    new AlertDialog.Builder(MainActivity.this)
                            .setMessage(R.string.dialog_continue_test_message)
                            .setPositiveButton(R.string.dialog_continue_test_yes_title, continueTestDialogListener)
                            .setNegativeButton(R.string.dialog_continue_test_no_title, continueTestDialogListener)
                            .create()
                            .show()
                    ;
                    return;
                }
                continueTest();
                break;
            case ACTION_BEFORE_CHECK_TEST:
                if (statistic == null) {
                    OnClickListener checkNonExisitngTestDialogListener = new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_NEGATIVE:
                                    break;
                                case DialogInterface.BUTTON_POSITIVE:
                                    startNewTest();
                                    break;
                                default:
                                    break;
                            }
                        }
                    };
                    new AlertDialog.Builder(MainActivity.this)
                            .setMessage(R.string.dialog_check_non_existing_test_message)
                            .setPositiveButton(
                                    R.string.dialog_check_non_existing_test_yes_title,
                                    checkNonExisitngTestDialogListener)
                            .setNegativeButton(
                                    R.string.dialog_check_non_existing_test_no_title,
                                    checkNonExisitngTestDialogListener)
                            .create()
                            .show();
                    return;
                }
                OnClickListener confirmCheckTestDialogListener = new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_NEGATIVE:
                                continueTest();
                                break;
                            case DialogInterface.BUTTON_POSITIVE:
                                checkTest();
                                break;
                            default:
                                break;
                        }
                    }
                };
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage(R.string.dialog_check_test_confirm_message)
                        .setPositiveButton(
                                R.string.dialog_check_test_confirm_yes_title,
                                confirmCheckTestDialogListener)
                        .setNegativeButton(
                                R.string.dialog_check_test_confirm_no_title,
                                confirmCheckTestDialogListener)
                        .create()
                        .show();
                break;
            default:
                break;
        }
    }

    private <T extends Fragment> void replaceFragment(Class<T> clazz, Object response) {
        Fragment fragment = null;
        if (clazz == TestFragment.class) {
            ArrayList<QuestionInfo> questionInfos = (ArrayList<QuestionInfo>) response;
            fragment = TestFragment.newInstance(questionInfos);
        } else if (clazz == StatFragment.class) {
            Map<Statistic, List<QuestionInfo>> questionInfos =
                    (Map<Statistic, List<QuestionInfo>>) response;
            fragment = StatFragment.newInstance(questionInfos);
        } else if (clazz == MainFragment.class) {
            fragment = MainFragment.newInstance();
        } else if (clazz == TheoryFragment.class) {
            fragment = TheoryFragment.newInstance();
        } else if (clazz == RulesFragment.class) {
            fragment = RulesFragment.newInstance();
        } else {
            throw new IllegalArgumentException("Illegal Fragment class");
        }
        FragmentManager fm = getSupportFragmentManager();
        //to avoid runtime error "cannot perform this action after onSaveInstanceState"
        if (!fm.isStateSaved()) {
            fm
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();
        }
    }
}