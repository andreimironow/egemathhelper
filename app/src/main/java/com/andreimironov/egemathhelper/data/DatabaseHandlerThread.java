package com.andreimironov.egemathhelper.data;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import com.andreimironov.egemathhelper.models.answers.Answer;

import java.util.Map;

public class DatabaseHandlerThread extends HandlerThread {
    private static final String TAG = "DatabaseHandlerThread";
    public static final int REQUEST_GET_CURRENT_STATISTIC = 0;
    public static final int REQUEST_INSERT_STATISTIC_AND_GET_CURRENT_QUESTION_INFOS = 1;
    public static final int REQUEST_UPDATE_AND_INSERT_STATISTIC_AND_GET_CURRENT_QUESTION_INFOS = 2;
    public static final int REQUEST_GET_CURRENT_QUESTION_INFOS = 3;
    public static final int REQUEST_UPDATE_STATISTIC_AND_GET_ALL_QUESTION_INFOS = 4;
    public static final int REQUEST_UPDATE_STATISTIC_ENTRY = 5;
    public static final int REQUEST_GET_ALL_QUESTION_INFOS = 6;

    private DatabaseHandler mRequestHandler;
    private Handler mResponseHandler;
    private DatabaseHandlerThreadListener mDatabaseHandlerThreadListener;
    private Database mDatabase;

    public DatabaseHandlerThread(
            Handler responseHandler,
            Context context,
            DatabaseHandlerThreadListener databaseHandlerThreadListener) {
        super(TAG);
        mResponseHandler = responseHandler;
        mDatabase = Database.getDatabase(context);
        mDatabaseHandlerThreadListener = databaseHandlerThreadListener;
    }

    public interface DatabaseHandlerThreadListener {
        void onResponseReceived(int request, int action, Object response);
    }

    private class DatabaseHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            try {
                int request = msg.what;
                Object object = msg.obj;
                Class objectClass = object.getClass();
                final int action = (int) objectClass.getDeclaredField("mAction").get(object);
                Object response = null;
                switch(request) {
                    case REQUEST_GET_CURRENT_STATISTIC:
                        response = mDatabase.getCurrentStatistic();
                        break;
                    case REQUEST_INSERT_STATISTIC_AND_GET_CURRENT_QUESTION_INFOS:
                        mDatabase.insertStatistic();
                        response = mDatabase.getCurrentQuestionInfos();
                        break;
                    case REQUEST_UPDATE_AND_INSERT_STATISTIC_AND_GET_CURRENT_QUESTION_INFOS:
                        mDatabase.updateCurrentStatistic();
                        mDatabase.insertStatistic();
                        response = mDatabase.getCurrentQuestionInfos();
                        break;
                    case REQUEST_GET_CURRENT_QUESTION_INFOS:
                        response = mDatabase.getCurrentQuestionInfos();
                        break;
                    case REQUEST_UPDATE_STATISTIC_AND_GET_ALL_QUESTION_INFOS:
                        mDatabase.updateCurrentStatistic();
                        response = mDatabase.getQuestionInfosMap();
                        break;
                    case REQUEST_UPDATE_STATISTIC_ENTRY:
                        long id = (long) objectClass.getDeclaredField("mId").get(object);
                        Map<Integer, Answer> newUserAnswers =
                                (Map<Integer, Answer>) objectClass
                                        .getDeclaredField("mNewUserAnswers")
                                        .get(object);
                        boolean areNewUserAnswersValid = (boolean) objectClass
                                .getDeclaredField("mAreNewUserAnswersValid")
                                .get(object);
                        mDatabase.updateStatisticEntry(id, newUserAnswers);
                        response = areNewUserAnswersValid;
                        break;
                    case REQUEST_GET_ALL_QUESTION_INFOS:
                        response = mDatabase.getQuestionInfosMap();
                        break;
                    default:
                        break;
                }
                Object finalResponse = response;
                mResponseHandler.post(
                        new Runnable() {
                            @Override
                            public void run() {
                                mDatabaseHandlerThreadListener
                                        .onResponseReceived(request, action, finalResponse);
                            }
                        }
                );
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onLooperPrepared() {
        super.onLooperPrepared();
        mRequestHandler = new DatabaseHandler();
    }

    public void obtainRequest(int request, Object object) {
        mRequestHandler.obtainMessage(request, object).sendToTarget();
    }
}
