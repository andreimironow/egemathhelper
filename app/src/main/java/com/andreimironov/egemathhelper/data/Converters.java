package com.andreimironov.egemathhelper.data;

import com.andreimironov.egemathhelper.models.answers.Answer;
import com.andreimironov.egemathhelper.models.answers.AnswerTypes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Map;

import androidx.room.TypeConverter;

public class Converters {
    public static final Gson GSON = new GsonBuilder()
            .registerTypeHierarchyAdapter(Answer.class, new AnswerConverter())
            .create();

    private static class AnswerConverter
            implements JsonSerializer<Answer>, JsonDeserializer<Answer>
    {
        @Override
        public JsonElement serialize(
                Answer answer,
                Type typeOfSrc,
                JsonSerializationContext context
        ) {
            JsonObject object = new JsonObject();
            object.addProperty("type", answer.getAnswerType().getValue());
            object.addProperty("answer", answer.getAnswer());
            return object;
        }

        @Override
        public Answer deserialize(
                JsonElement json,
                Type typeOfT,
                JsonDeserializationContext context
        ) throws JsonParseException {
            JsonObject object = json.getAsJsonObject();
            AnswerTypes type = AnswerTypes.valueOf(object.get("type").getAsInt());
            String answer = object.get("answer").getAsString();
            return Answer.newInstance(type, answer);
        }
    }

    @TypeConverter
    public static String convertAnswersToString(Map<Integer, Answer> answers) {
        return GSON.toJson(
                answers,
                new TypeToken<Map<Integer, Answer>>() {}.getType()
        );
    }

    @TypeConverter
    public static Map<Integer, Answer> convertStringToAnswers(String answers) {
        return GSON.fromJson(
                answers,
                new TypeToken<Map<Integer, Answer>>() {}.getType()
        );
    }
}
