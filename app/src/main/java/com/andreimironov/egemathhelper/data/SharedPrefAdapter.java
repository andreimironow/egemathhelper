package com.andreimironov.egemathhelper.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.andreimironov.egemathhelper.R;

import java.util.HashMap;
import java.util.Map;

public class SharedPrefAdapter {
    private static final String KEY_MENU_ITEM = "menu item";
    private static final String SHARED_PREFERENCES_NAME = "my shared preferences";

    private static SharedPrefAdapter sSharedPrefAdapter;
    public static SharedPrefAdapter getSharedPrefAdapter(Context context) {
        if (sSharedPrefAdapter == null) {
            sSharedPrefAdapter = new SharedPrefAdapter(context.getApplicationContext());
        }
        return sSharedPrefAdapter;
    }

    private static SharedPreferences mSharedPreferences;

    private SharedPrefAdapter(Context context) {
        mSharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public void putMenuItemId(int menuItemId) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        int menuItemValue = MenuItems.getMenuItemFromId(menuItemId).getValue();
        editor.putInt(KEY_MENU_ITEM, menuItemValue);
        editor.apply();
    }

    public Integer getMenuItemId() {
        int defaultValue = -1;
        int menuItemValue = mSharedPreferences.getInt(KEY_MENU_ITEM, defaultValue);
        return menuItemValue == defaultValue
                ? null
                : MenuItems.getMenuItemFromValue(menuItemValue).getId()
        ;
    }

    private enum MenuItems {
        MAIN(R.id.menu_nav_main, 0),
        NEW_TEST(R.id.menu_nav_new_test, 1),
        CONTINUE_TEST(R.id.menu_nav_continue_test, 2),
        CHECK_TEST(R.id.menu_nav_check_test, 3),
        RULES(R.id.menu_nav_rules, 4),
        THEORY(R.id.menu_nav_theory, 5),
        STAT(R.id.menu_nav_stat, 6);

        private int mId;
        private int mValue;

        public int getId() {
            return mId;
        }

        public int getValue() {
            return mValue;
        }

        MenuItems(int id, int value) {
            mId = id;
            mValue = value;
        }

        private static Map<Integer, MenuItems> ids = new HashMap<>();
        private static Map<Integer, MenuItems> values = new HashMap<>();
        static {
            for (MenuItems item: MenuItems.values()) {
                ids.put(item.mId, item);
                values.put(item.mValue, item);
            }
        }
        public static MenuItems getMenuItemFromId(int id) {
            return ids.get(id);
        }
        public static MenuItems getMenuItemFromValue(int value) {
            return values.get(value);
        }
    }
}
