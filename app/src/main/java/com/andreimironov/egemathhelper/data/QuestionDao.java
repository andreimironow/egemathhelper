package com.andreimironov.egemathhelper.data;

import com.andreimironov.egemathhelper.models.Question;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface QuestionDao {
    @Insert
    List<Long> insertQuestions(List<Question> questions);

    @Query(
            "SELECT * FROM " + Question.TABLE_NAME +
                    " WHERE " + Question.COLS.INDEX + " = :index" +
                    " ORDER BY RANDOM() LIMIT 1")
    Question getRandomQuestion(int index);

    @Query("SELECT * FROM " + Question.TABLE_NAME + " WHERE " + Question.COLS.ID + " = :id")
    Question getQuestion(long id);
}
