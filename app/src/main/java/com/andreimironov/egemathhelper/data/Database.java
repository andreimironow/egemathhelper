package com.andreimironov.egemathhelper.data;

import android.content.Context;

import com.andreimironov.egemathhelper.R;
import com.andreimironov.egemathhelper.models.answers.Answer;
import com.andreimironov.egemathhelper.models.answers.AnswerTypes;
import com.andreimironov.egemathhelper.models.Question;
import com.andreimironov.egemathhelper.models.QuestionInfo;
import com.andreimironov.egemathhelper.models.Statistic;
import com.andreimironov.egemathhelper.models.StatisticEntry;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

@androidx.room.Database(
        entities = {Question.class, Statistic.class, StatisticEntry.class},
        version = 1)
@TypeConverters(Converters.class)
public abstract class Database extends RoomDatabase {
    public static final String DATABASE_NAME = "database";
    public static final int TEST_SIZE = 19;

    private static Database sDatabase;

    public static Database getDatabase(Context context) {
        if (sDatabase == null) {
            sDatabase = Room
                    .databaseBuilder(context.getApplicationContext(), Database.class, DATABASE_NAME)
                    .addCallback(new RoomDatabase.Callback() {
                        @Override
                        public void onCreate(@NonNull SupportSQLiteDatabase db) {
                            super.onCreate(db);
                            Executors.newSingleThreadExecutor().execute(new Runnable() {
                                @Override
                                public void run() {
                                    sDatabase.insertQuestions(context.getApplicationContext());
                                }
                            });
                        }
                    })
                    .build();
        }
        return sDatabase;
    }

    public abstract QuestionDao getQuestionDao();
    public abstract StatisticDao getStatisticDao();
    public abstract StatisticEntryDao getStatisticEntryDao();

    public void insertQuestions(Context context) {
        List<Question> questions = new ArrayList<>();
        questions.add(Question.newInstance(
                1,
                context,
                R.string.var_1_question_1,
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_1_question_1_answer)));
        questions.add(Question.newInstance(
                2,
                context,
                R.string.var_1_question_2,
                "2a.png",
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_1_question_2_answer)));
        questions.add(Question.newInstance(
                3,
                context,
                R.string.var_1_question_3,
                "3a.png",
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_1_question_3_answer)));
        questions.add(Question.newInstance(
                4,
                context,
                R.string.var_1_question_4,
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_1_question_4_answer)));
        questions.add(Question.newInstance(
                5,
                context,
                R.string.var_1_question_5,
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_1_question_5_answer)));
        questions.add(Question.newInstance(
                6,
                context,
                R.string.var_1_question_6,
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_1_question_6_answer)));
        questions.add(Question.newInstance(
                7,
                context,
                R.string.var_1_question_7,
                "7a.png",
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_1_question_7_answer)));
        questions.add(Question.newInstance(
                8,
                context,
                R.string.var_1_question_8,
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_1_question_8_answer)));
        questions.add(Question.newInstance(
                9,
                context,
                R.string.var_1_question_9,
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_1_question_9_answer)));
        questions.add(Question.newInstance(
                10,
                context,
                R.string.var_1_question_10,
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_1_question_10_answer)));
        questions.add(Question.newInstance(
                11,
                context,
                R.string.var_1_question_11,
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_1_question_11_answer)));
        questions.add(Question.newInstance(
                12,
                context,
                R.string.var_1_question_12,
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_1_question_12_answer)));
        questions.add(Question.newInstance(
                13,
                context,
                R.string.var_1_question_13,
                1,
                Answer.newInstance(AnswerTypes.MULTIPLE_TRIG_NUMBERS, context, R.string.var_1_question_13_answerA),
                Answer.newInstance(AnswerTypes.MULTIPLE_NUMBERS, context, R.string.var_1_question_13_answerB)));
        questions.add(Question.newInstance(
                14,
                context,
                R.string.var_1_question_14,
                1,
                Answer.newInstance(AnswerTypes.SINGLE_NUMBER, context, R.string.var_1_question_14_answer)));
        questions.add(Question.newInstance(
                15,
                context,
                R.string.var_1_question_15,
                1,
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, context, R.string.var_1_question_15_answer)));
        questions.add(Question.newInstance(
                16,
                context,
                R.string.var_1_question_16,
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_1_question_16_answer)));
        questions.add(Question.newInstance(
                17,
                context,
                R.string.var_1_question_17,
                "17a.png",
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_1_question_17_answer)));
        questions.add(Question.newInstance(
                18,
                context,
                R.string.var_1_question_18,
                1,
                Answer.newInstance(AnswerTypes.MULTIPLE_NUMBERS, context, R.string.var_1_question_18_answer)));
        questions.add(Question.newInstance(
                19,
                context,
                R.string.var_1_question_19,
                1,
                Answer.newInstance(AnswerTypes.BOOLEAN, context, R.string.var_1_question_19_answerA),
                Answer.newInstance(AnswerTypes.BOOLEAN, context, R.string.var_1_question_19_answerB),
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_1_question_19_answerC)));
        questions.add(Question.newInstance(
                1,
                context,
                R.string.var_2_question_1,
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_2_question_1_answer)));
        questions.add(Question.newInstance(
                2,
                context,
                R.string.var_2_question_2,
                "2b.png",
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_2_question_2_answer)));
        questions.add(Question.newInstance(
                3,
                context,
                R.string.var_2_question_3,
                "3b.png",
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_2_question_3_answer)));
        questions.add(Question.newInstance(
                4,
                context,
                R.string.var_2_question_4,
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_2_question_4_answer)));
        questions.add(Question.newInstance(
                5,
                context,
                R.string.var_2_question_5,
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_2_question_5_answer)));
        questions.add(Question.newInstance(
                6,
                context,
                R.string.var_2_question_6,
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_2_question_6_answer)));
        questions.add(Question.newInstance(
                7,
                context,
                R.string.var_2_question_7,
                "7b.png",
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_2_question_7_answer)));
        questions.add(Question.newInstance(
                8,
                context,
                R.string.var_2_question_8,
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_2_question_8_answer)));
        questions.add(Question.newInstance(
                9,
                context,
                R.string.var_2_question_9,
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_2_question_9_answer)));
        questions.add(Question.newInstance(
                10,
                context,
                R.string.var_2_question_10,
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_2_question_10_answer)));
        questions.add(Question.newInstance(
                11,
                context,
                R.string.var_2_question_11,
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_2_question_11_answer)));
        questions.add(Question.newInstance(
                12,
                context,
                R.string.var_2_question_12,
                1,
                Answer.newInstance(AnswerTypes.DECIMAL, context, R.string.var_2_question_12_answer)));

        getQuestionDao().insertQuestions(questions);
    }

    public void insertStatistic() {
        Statistic statistic = new Statistic();
        long statisticId = getStatisticDao().insertStatistic(statistic);
        List<StatisticEntry> statisticEntries = new ArrayList<>();
        for (int index = 1; index <= TEST_SIZE; index++) {
            Question question = getQuestionDao().getRandomQuestion(index);
            Map<Integer, Answer> userAnswers = new HashMap<>();
            for (Map.Entry<Integer, Answer> entry: question.getAnswers().entrySet()) {
                Integer key = entry.getKey();
                AnswerTypes type = entry.getValue().getAnswerType();
                userAnswers.put(key, Answer.emptyAnswer(type));
            }
            statisticEntries.add(
                    new StatisticEntry(question.getId(), statisticId, userAnswers));
        }
        getStatisticEntryDao().insertStatisticEntries(statisticEntries);
    }

    public void updateCurrentStatistic() {
        Statistic statistic = getStatisticDao().getNotFinishedStatistic();
        statistic.setEndTime(new Date().getTime());
        getStatisticDao().updateStatistic(statistic);
    }

    public void updateStatisticEntry(long id, Map<Integer, Answer> userAnswers) {
        Statistic statistic = getStatisticDao().getNotFinishedStatistic();
        StatisticEntry statisticEntry = getStatisticEntryDao().getStatisticEntry(statistic.getId(), id);
        statisticEntry.setUserAnswers(userAnswers);
        getStatisticEntryDao().updateStatisticEntry(statisticEntry);
    }

    public Statistic getCurrentStatistic() {
        return getStatisticDao().getNotFinishedStatistic();
    }

    public List<QuestionInfo> getCurrentQuestionInfos() {
        return getQuestionInfos(getCurrentStatistic());
    }

    private List<QuestionInfo> getQuestionInfos(Statistic statistic) {
        List<QuestionInfo> questionInfos = new ArrayList<>();
        List<StatisticEntry> statisticEntries =
                getStatisticEntryDao().getStatisticEntries(statistic.getId());
        for (StatisticEntry statisticEntry: statisticEntries) {
            Question question = getQuestionDao().getQuestion(statisticEntry.getQuestionId());
            QuestionInfo questionInfo = new QuestionInfo(question, statisticEntry.getUserAnswers());
            questionInfos.add(questionInfo);
        }
        return questionInfos;
    }

    public Map<Statistic, List<QuestionInfo>> getQuestionInfosMap() {
        Map<Statistic, List<QuestionInfo>> questionInfosMap = new HashMap<>();
        List<Statistic> statistics = getStatisticDao().getAllFinishedStatistics();
        for (Statistic statistic: statistics) {
            List<QuestionInfo> questionInfos = getQuestionInfos(statistic);
            questionInfosMap.put(statistic, questionInfos);
        }
        return questionInfosMap;
    }
}
