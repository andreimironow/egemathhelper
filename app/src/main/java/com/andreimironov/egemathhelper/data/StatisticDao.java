package com.andreimironov.egemathhelper.data;

import com.andreimironov.egemathhelper.models.Statistic;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface StatisticDao {
    @Insert
    long insertStatistic(Statistic statistic);
    
    @Query(
            "SELECT * FROM " + Statistic.TABLE_NAME +
                    " WHERE " + Statistic.COLS.END_TIME + " IS NULL")
    Statistic getNotFinishedStatistic();

    @Query(
            "SELECT * FROM " + Statistic.TABLE_NAME +
                    " WHERE " + Statistic.COLS.END_TIME + " IS NOT NULL")
    List<Statistic> getAllFinishedStatistics();

    @Update
    void updateStatistic(Statistic statistic);
}
