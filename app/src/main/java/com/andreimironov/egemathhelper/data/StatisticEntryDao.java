package com.andreimironov.egemathhelper.data;

import com.andreimironov.egemathhelper.models.StatisticEntry;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface StatisticEntryDao {
    @Insert
    List<Long> insertStatisticEntries(List<StatisticEntry> statisticEntries);

    @Query(
            "SELECT * FROM " + StatisticEntry.TABLE_NAME +
                    " WHERE " + StatisticEntry.COLS.STATISTIC_ID + " = :statisticId")
    List<StatisticEntry> getStatisticEntries(long statisticId);

    @Query(
            "SELECT * FROM " + StatisticEntry.TABLE_NAME +
                    " WHERE " + StatisticEntry.COLS.STATISTIC_ID + " = :statisticId" +
                    " AND " + StatisticEntry.COLS.QUESTION_ID + " = :questionId")
    StatisticEntry getStatisticEntry(long statisticId, long questionId);

    @Update
    void updateStatisticEntry(StatisticEntry statisticEntry);
}
