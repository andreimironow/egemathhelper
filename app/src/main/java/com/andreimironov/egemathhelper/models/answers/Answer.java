package com.andreimironov.egemathhelper.models.answers;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public abstract class Answer implements Comparable {
    public static Answer newInstance(
            @NonNull AnswerTypes answerType,
            @NonNull Context context,
            @NonNull int answerResId
    ) {
        String answer = context.getResources().getString(answerResId);
        return newInstance(answerType, answer);
    }

    public static Answer newInstance(@NonNull AnswerTypes answerType, @NonNull String answer) {
        switch (answerType) {
            case DECIMAL:
                return new DecimalAnswer(answer);
            case BOOLEAN:
                return new BooleanAnswer(answer);
            case SINGLE_NUMBER:
                return new SingleNumberAnswer(answer);
            case MULTIPLE_NUMBERS:
                return new MultipleNumbersAnswer(answer);
            case SINGLE_TRIG_NUMBER:
                return new SingleTrigNumberAnswer(answer);
            case MULTIPLE_TRIG_NUMBERS:
                return new MultipleTrigNumbersAnswer(answer);
            case SINGLE_SET:
                return new SingleSetAnswer(answer);
            case MULTIPLE_SETS:
                return new MultipleSetsAnswer(answer);
            default:
                return null;
        }
    }

    public static Answer emptyAnswer(@NonNull AnswerTypes answerType) {
        return newInstance(answerType, "");
    }

    public abstract boolean isValid();

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof Answer) {
            return equals((Answer)obj);
        }
        return false;
    }

    public abstract boolean equals(Answer answer);

    protected String mAnswer;
    protected AnswerTypes mAnswerType;

    public AnswerTypes getAnswerType() {
        return mAnswerType;
    }

    public String getAnswer() {
        return mAnswer;
    }

    public void setAnswer(String answer) {
        mAnswer = answer;
    }

    public boolean isEmpty() {
        return mAnswer.equals("");
    }
}