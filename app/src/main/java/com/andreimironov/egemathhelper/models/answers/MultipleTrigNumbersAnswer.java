package com.andreimironov.egemathhelper.models.answers;

import android.content.Context;
import android.content.res.Resources;

import com.andreimironov.egemathhelper.R;

import java.util.Arrays;

public class MultipleTrigNumbersAnswer extends Answer {
    protected MultipleTrigNumbersAnswer(String answer) {
        mAnswer = answer;
        mAnswerType = AnswerTypes.MULTIPLE_TRIG_NUMBERS;
        String[] numbers = mAnswer.split(";");
        Arrays.sort(numbers);
        mNumbers = new Answer[numbers.length];
        for (int i = 0; i < mNumbers.length; i++) {
            mNumbers[i] = Answer.newInstance(
                    AnswerTypes.SINGLE_TRIG_NUMBER,numbers[i]);
        }
    }

    private Answer[] mNumbers;

    @Override
    public boolean isValid() {
        if (isEmpty()) {
            return true;
        }
        for (Answer answer: mNumbers) {
            if (!answer.isValid()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean equals(Answer answer) {
        if (answer.getAnswerType() != AnswerTypes.MULTIPLE_TRIG_NUMBERS) {
            return false;
        }
        if (!isValid() || !answer.isValid()) {
            return false;
        }
        if (isEmpty() || answer.isEmpty()) {
            return isEmpty() & answer.isEmpty();
        }
        return Arrays.equals(mNumbers, ((MultipleTrigNumbersAnswer) answer).mNumbers);
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
