package com.andreimironov.egemathhelper.models.answers;

import android.content.Context;
import android.content.res.Resources;

import com.andreimironov.egemathhelper.R;

class SingleSetAnswer extends Answer {
    protected SingleSetAnswer(String answer) {
        mAnswer = answer;
        mAnswerType = AnswerTypes.SINGLE_SET;
        if (answer.matches("[(\\[].*;.*[)\\]]")) {
            String[] string = answer.split(";", 2);
            String leftPart = string[0];
            String rightPart = string[1];
            switch (leftPart.charAt(0)) {
                case '[':
                    mLeftIncluded = true;
                    break;
                case '(':
                    mLeftIncluded = false;
                    break;
                default:
                    break;
            }
            switch (rightPart.charAt(rightPart.length() - 1)) {
                case ']':
                    mRightIncluded = true;
                    break;
                case ')':
                    mRightIncluded = false;
                    break;
                default:
                    break;
            }
            mLeftBound = Answer.newInstance(AnswerTypes.SINGLE_NUMBER, leftPart.substring(1));
            mRightBound = Answer.newInstance(
                    AnswerTypes.SINGLE_NUMBER, rightPart.substring(0, rightPart.length() - 1));
        } else {
            mLeftIncluded = true;
            mRightIncluded = true;
            mLeftBound = Answer.newInstance(AnswerTypes.SINGLE_NUMBER, answer);
            mRightBound = mLeftBound;
        }
    }

    private Answer mLeftBound;
    private Boolean mLeftIncluded;
    private Answer mRightBound;
    private Boolean mRightIncluded;

    @Override
    public boolean isValid() {
        if (isEmpty()) {
            return true;
        }
        return mLeftBound.isValid() && mRightBound.isValid();
    }

    @Override
    public boolean equals(Answer answer) {
        if (answer.getAnswerType() != AnswerTypes.SINGLE_SET) {
            return false;
        }
        SingleSetAnswer singleSetAnswer = (SingleSetAnswer) answer;
        if (!isValid() || !singleSetAnswer.isValid()) {
            return false;
        }
        if (isEmpty() || singleSetAnswer.isEmpty()) {
            return isEmpty() & singleSetAnswer.isEmpty();
        }
        if (!mLeftIncluded.equals(singleSetAnswer.mLeftIncluded)) {
            return false;
        }
        if (!mRightIncluded.equals(singleSetAnswer.mRightIncluded)) {
            return false;
        }
        if (!mLeftBound.equals(singleSetAnswer.mLeftBound)) {
            return false;
        }
        if (!mRightBound.equals(singleSetAnswer.mRightBound)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof SingleSetAnswer) {
            SingleSetAnswer answer = (SingleSetAnswer) o;
            if ((mLeftIncluded && answer.mLeftIncluded) ||
                    (!mLeftIncluded && !answer.mLeftIncluded)) {
                if ((mRightIncluded && answer.mRightIncluded) ||
                        (!mRightIncluded && !answer.mRightIncluded)) {
                    int compare = mLeftBound.compareTo(answer.mLeftBound);
                    if (compare == 0) {
                        return mRightBound.compareTo(answer.mRightBound);
                    }
                    return compare;
                }
                return mRightIncluded ? 1 : -1;
            }
            return mLeftIncluded ? 1 : -1;
        }
        return 0;
    }
}
