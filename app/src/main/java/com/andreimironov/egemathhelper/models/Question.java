package com.andreimironov.egemathhelper.models;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.andreimironov.egemathhelper.R;
import com.andreimironov.egemathhelper.data.Converters;
import com.andreimironov.egemathhelper.models.answers.Answer;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.StringRes;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = Question.TABLE_NAME)
public class Question implements Parcelable {
    public static final String TABLE_NAME = "questions";

    public static final class COLS {
        public static final String ID = "id";
        public static final String INDEX = "question_index";
        public static final String WORDING = "wording";
        public static final String IMAGE = "image";
        public static final String DIFFICULTY = "difficulty";
        public static final String ANSWERS = "answers";
    }

    protected Question(Parcel in) {
        mId = in.readLong();
        mIndex = in.readInt();
        mWording = in.readString();
        mImage = in.createByteArray();
        mDifficulty = in.readInt();
        mAnswers = Converters.convertStringToAnswers(in.readString());
    }

    public static final Creator<Question> CREATOR = new Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel in) {
            return new Question(in);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeInt(mIndex);
        dest.writeString(mWording);
        dest.writeByteArray(mImage);
        dest.writeInt(mDifficulty);
        dest.writeString(Converters.convertAnswersToString(mAnswers));
    }

    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = COLS.ID) private long mId;
    @ColumnInfo(name = COLS.INDEX) private int mIndex;
    @ColumnInfo(name = COLS.WORDING) private String mWording;
    @ColumnInfo(name = COLS.IMAGE) private byte[] mImage;
    @ColumnInfo(name = COLS.DIFFICULTY) private int mDifficulty;
    @ColumnInfo(name = COLS.ANSWERS) private Map<Integer, Answer> mAnswers;

    public Question(
            int index,
            String wording,
            byte[] image,
            int difficulty,
            Map<Integer, Answer> answers) {
        mId = 0;
        mIndex = index;
        mWording = wording;
        mImage = image;
        mDifficulty = difficulty;
        mAnswers = answers;
    }

    public static Question newInstance(
            int index, Context context, int wordingResId, int difficulty, Answer... answers) {
        String wording = context.getResources().getString(wordingResId);
        Map<Integer, Answer> answersMap = new HashMap<>();
        int i = 0;
        for (Answer answer: answers) {
            answersMap.put(i, answer);
            i++;
        }
        return new Question(index, wording, null, difficulty, answersMap);
    }

    public static Question newInstance(
            int index,
            Context context,
            int wordingResId,
            String imagePath,
            int difficulty,
            Answer... answers) {
        String wording = context.getResources().getString(wordingResId);
        byte[] image = null;
        try (InputStream stream = context.getAssets().open(imagePath)) {
            image = new byte[stream.available()];
            stream.read(image, 0, stream.available());
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        Map<Integer, Answer> answersMap = new HashMap<>();
        int i = 0;
        for (Answer answer: answers) {
            answersMap.put(i, answer);
            i++;
        }
        return new Question(index, wording, image, difficulty, answersMap);
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public int getIndex() {
        return mIndex;
    }

    public void setIndex(int index) {
        mIndex = index;
    }

    public String getWording() {
        return mWording;
    }

    public void setWording(String wording) {
        mWording = wording;
    }

    public byte[] getImage() {
        return mImage;
    }

    public void setImage(byte[] image) {
        mImage = image;
    }

    public int getDifficulty() {
        return mDifficulty;
    }

    public void setDifficulty(int difficulty) {
        mDifficulty = difficulty;
    }

    public Map<Integer, Answer> getAnswers() {
        return mAnswers;
    }

    public void setAnswers(Map<Integer, Answer> answers) {
        mAnswers = answers;
    }
}
