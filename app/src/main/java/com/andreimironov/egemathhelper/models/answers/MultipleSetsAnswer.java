package com.andreimironov.egemathhelper.models.answers;

import android.content.Context;
import android.content.res.Resources;

import com.andreimironov.egemathhelper.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MultipleSetsAnswer extends Answer {
    protected MultipleSetsAnswer(String answer) {
        mAnswer = answer;
        mAnswerType = AnswerTypes.MULTIPLE_SETS;
        List<String> singleSets = new ArrayList<>();
        String string = answer.replace("$", "");
        Pattern pattern = Pattern.compile("[\\[(][^\\[\\]()]+;[^\\[\\]()]+[\\])]");
        Matcher matcher = pattern.matcher(string);
        while (matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();
            singleSets.add(string.substring(start, end));
        }
        string = matcher.replaceAll("");
        for (String singleNumber: string.split(";")) {
            if (!singleNumber.equals("")) {
                singleSets.add(singleNumber);
            }
        }
        mSingleSets = new Answer[singleSets.size()];
        for (int i = 0; i < mSingleSets.length; i++) {
            mSingleSets[i] = Answer.newInstance(AnswerTypes.SINGLE_SET,singleSets.get(i));
        }
        Arrays.sort(mSingleSets);
    }

    public Answer[] getSingleSets() {
        return mSingleSets;
    }

    private Answer[] mSingleSets;

    @Override
    public boolean isValid() {
        if (isEmpty()) {
            return true;
        }
        for (Answer answer: mSingleSets) {
            if (!answer.isValid()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean equals(Answer answer) {
        if (answer.getAnswerType() != AnswerTypes.MULTIPLE_SETS) {
            return false;
        }
        if (!isValid() || !answer.isValid()) {
            return false;
        }
        if (isEmpty() || answer.isEmpty()) {
            return isEmpty() & answer.isEmpty();
        }
        return Arrays.equals(mSingleSets, ((MultipleSetsAnswer) answer).mSingleSets);
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
