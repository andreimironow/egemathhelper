package com.andreimironov.egemathhelper.models;

import com.andreimironov.egemathhelper.models.answers.Answer;

import java.util.Map;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = StatisticEntry.TABLE_NAME)
public class StatisticEntry {
    public static final String TABLE_NAME = "statistic_entries";
    public static final class COLS {
        public static final String STATISTIC_ID = "statistic_id";
        public static final String QUESTION_ID = "question_id";
        public static final String USER_ANSWERS = "user_answers";
    }

    @PrimaryKey(autoGenerate = true) private long mId;
    @ColumnInfo(name = COLS.STATISTIC_ID) private long mStatisticId;
    @ColumnInfo(name = COLS.QUESTION_ID) private long mQuestionId;
    @ColumnInfo(name = COLS.USER_ANSWERS) private Map<Integer, Answer> mUserAnswers;

    public StatisticEntry(long questionId, long statisticId, Map<Integer, Answer> userAnswers) {
        mId = 0;
        mQuestionId = questionId;
        mStatisticId = statisticId;
        mUserAnswers = userAnswers;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public long getStatisticId() {
        return mStatisticId;
    }

    public void setStatisticId(long statisticId) {
        mStatisticId = statisticId;
    }

    public long getQuestionId() {
        return mQuestionId;
    }

    public void setQuestionId(long questionId) {
        mQuestionId = questionId;
    }

    public Map<Integer, Answer> getUserAnswers() {
        return mUserAnswers;
    }

    public void setUserAnswers(Map<Integer, Answer> userAnswers) {
        mUserAnswers = userAnswers;
    }
}