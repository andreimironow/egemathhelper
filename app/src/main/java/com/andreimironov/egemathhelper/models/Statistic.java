package com.andreimironov.egemathhelper.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = Statistic.TABLE_NAME)
public class Statistic implements Parcelable {
    public static final String TABLE_NAME = "statistics";

    protected Statistic(Parcel in) {
        mId = in.readLong();
        mStartTime = in.readLong();
        if (in.readByte() == 0) {
            mEndTime = null;
        } else {
            mEndTime = in.readLong();
        }
    }

    public static final Creator<Statistic> CREATOR = new Creator<Statistic>() {
        @Override
        public Statistic createFromParcel(Parcel in) {
            return new Statistic(in);
        }

        @Override
        public Statistic[] newArray(int size) {
            return new Statistic[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeLong(mStartTime);
        if (mEndTime == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(mEndTime);
        }
    }

    public static final class COLS {
        public static final String START_TIME = "start_time";
        public static final String END_TIME = "end_time";
    }

    @PrimaryKey(autoGenerate = true) private long mId;
    @ColumnInfo(name = COLS.START_TIME) private long mStartTime;
    @ColumnInfo(name = COLS.END_TIME) private Long mEndTime;

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public long getStartTime() {
        return mStartTime;
    }

    public void setStartTime(long startTime) {
        mStartTime = startTime;
    }

    public Long getEndTime() {
        return mEndTime;
    }

    public void setEndTime(Long endTime) {
        mEndTime = endTime;
    }

    public Statistic() {
        this.mId = 0;
        this.mStartTime = new Date().getTime();
        this.mEndTime = null;
    }
}
