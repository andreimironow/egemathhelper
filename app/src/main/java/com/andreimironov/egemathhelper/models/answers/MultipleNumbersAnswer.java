package com.andreimironov.egemathhelper.models.answers;

import android.content.Context;
import android.content.res.Resources;

import com.andreimironov.egemathhelper.R;

import java.util.Arrays;

public class MultipleNumbersAnswer extends Answer {
    protected MultipleNumbersAnswer(String answer) {
        mAnswer = answer;
        mAnswerType = AnswerTypes.MULTIPLE_NUMBERS;
        String[] numbers = mAnswer.split(";");
        mSingleNumbers = new Answer[numbers.length];
        for(int i = 0; i < mSingleNumbers.length; i++) {
            mSingleNumbers[i] = Answer.newInstance(AnswerTypes.SINGLE_NUMBER, numbers[i]);
        }
        Arrays.sort(mSingleNumbers);
    }

    private Answer[] mSingleNumbers;

    @Override
    public boolean isValid() {
        if (isEmpty()) {
            return true;
        }
        for (Answer answer: mSingleNumbers) {
            if (!answer.isValid()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean equals(Answer answer) {
        if (answer.getAnswerType() != AnswerTypes.MULTIPLE_NUMBERS) {
            return false;
        }
        if (!isValid() || !answer.isValid()) {
            return false;
        }
        if (isEmpty() || answer.isEmpty()) {
            return isEmpty() & answer.isEmpty();
        }
        return Arrays.equals(mSingleNumbers, ((MultipleNumbersAnswer) answer).mSingleNumbers);
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
