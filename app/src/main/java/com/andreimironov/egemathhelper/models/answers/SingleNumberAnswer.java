package com.andreimironov.egemathhelper.models.answers;

import android.content.Context;

import com.andreimironov.egemathhelper.R;

import java.util.Arrays;

public class SingleNumberAnswer extends Answer {
    protected SingleNumberAnswer(String answer) {
        mAnswer = answer;
        mAnswerType = AnswerTypes.SINGLE_NUMBER;
        String string = mAnswer
                .replace("+-", "-")
                .replace("-", "+-")
                .replace("+", ";");
        if (string.startsWith(";")) {
            string = string.substring(1);
        }
        mValues = string.split(";");
        Arrays.sort(mValues);
    }

    private String[] mValues;

    @Override
    public boolean isValid() {
        if (isEmpty()) {
            return true;
        }
        //if there is at least one of , ; [ ] symbols
        if (mAnswer.matches(".*[,;\\[\\]].*")) {
            return false;
        }
        //if there is ( but no )
        if (mAnswer.matches(".*\\([^)]*")) {
            return false;
        }
        //if there is ) but no (
        if (mAnswer.matches("[^\\(]*\\).*")) {
            return false;
        }
        //if there is infinity without sign
        if (mAnswer.indexOf("∞") != -1 && !mAnswer.matches(".*[\\+\\-]{1}∞.*")) {
            return false;
        }
        //if the root has no braces
        if (mAnswer.indexOf("√") != -1 && !mAnswer.matches(".*√\\{[0-9π/{}]+\\}.*")) {
            return false;
        }
        return true;
    }

    @Override
    public boolean equals(Answer answer) {
        if (answer.getAnswerType() != AnswerTypes.SINGLE_NUMBER) {
            return false;
        }
        if (!isValid() || !answer.isValid()) {
            return false;
        }
        if (isEmpty() || answer.isEmpty()) {
            return isEmpty() & answer.isEmpty();
        }
        return Arrays.equals(mValues, ((SingleNumberAnswer) answer).mValues);
    }

    @Override
    public int compareTo(Object object) {
        if (object instanceof SingleNumberAnswer) {
            SingleNumberAnswer answer = (SingleNumberAnswer) object;
            if (equals(answer)) {
                return 0;
            }
            int minLength = Math.min(mValues.length, answer.mValues.length);
            for (int i = 0; i < minLength; i++) {
                String mValue = mValues[i];
                String answerValue = answer.mValues[i];
                int compare = mValue.compareTo(answerValue);
                if ( compare == 0) {
                    continue;
                }
                return compare;
            }
            if (mValues.length > minLength) {
                return 1;
            }
            if (answer.mValues.length > minLength) {
                return -1;
            }
        }
        return 0;
    }
}
