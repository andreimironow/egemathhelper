package com.andreimironov.egemathhelper.models.answers;

import android.content.Context;
import android.content.res.Resources;

import com.andreimironov.egemathhelper.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public enum AnswerTypes {
    DECIMAL(0),
    BOOLEAN(1),
    SINGLE_NUMBER(2),
    MULTIPLE_NUMBERS(3),
    SINGLE_TRIG_NUMBER(4),
    MULTIPLE_TRIG_NUMBERS(5),
    SINGLE_SET(6),
    MULTIPLE_SETS(7);

    private int value;

    public int getValue() {
        return value;
    }

    AnswerTypes(int value) {
        this.value = value;
    }

    public static String getAnswerRules(Context context, AnswerTypes answerType) {
        Resources resources = context.getResources();
        switch (answerType) {
            case DECIMAL:
                return resources.getString(R.string.decimal_answer_rules);
            case BOOLEAN:
                return resources.getString(R.string.boolean_answer_rules);
            case SINGLE_NUMBER:
                return resources.getString(R.string.single_number_answer_rules);
            case MULTIPLE_NUMBERS:
                return resources.getString(R.string.multiple_numbers_answer_rules) +
                        getAnswerRules(context, AnswerTypes.SINGLE_NUMBER);
            case SINGLE_TRIG_NUMBER:
                return resources.getString(R.string.single_trig_number_answer_rules);
            case MULTIPLE_TRIG_NUMBERS:
                return resources.getString(R.string.multiple_trig_numbers_answer_rules) +
                        getAnswerRules(context, AnswerTypes.SINGLE_TRIG_NUMBER);
            case SINGLE_SET:
                return resources.getString(R.string.single_set_answer_rules) +
                        getAnswerRules(context, AnswerTypes.SINGLE_NUMBER);
            case MULTIPLE_SETS:
                return resources.getString(R.string.multiple_sets_answer_rules) +
                        getAnswerRules(context, AnswerTypes.SINGLE_SET);
            default:
                return null;
        }
    }

    public List<Integer> getPossibleQuestions() {
        List<Integer> list = new ArrayList<>();
        switch (this) {
            case DECIMAL:
                for (int i = 1; i < 13; i++) {
                    list.add(i);
                }
                list.add(16);
                list.add(17);
                list.add(19);
                break;
            case BOOLEAN:
                list.add(19);
                break;
            case SINGLE_NUMBER:
                list.add(14);
                break;
            case MULTIPLE_NUMBERS:
                list.add(13);
                list.add(18);
                break;
            case SINGLE_TRIG_NUMBER:
                break;
            case MULTIPLE_TRIG_NUMBERS:
                list.add(13);
                break;
            case SINGLE_SET:
                break;
            case MULTIPLE_SETS:
                list.add(15);
                break;
            default:
                break;
        }
        return list;
    }

    private static HashMap<Integer, AnswerTypes> map;
    static {
        map = new HashMap<>();
        for (AnswerTypes type : AnswerTypes.values()) {
            map.put(type.value, type);
        }
    }
    public static AnswerTypes valueOf(int value) {
        return map.get(value);
    }
}
