package com.andreimironov.egemathhelper.models.answers;

import android.content.Context;

import com.andreimironov.egemathhelper.R;

class SingleTrigNumberAnswer extends Answer {
    protected SingleTrigNumberAnswer(String answer) {
        mAnswer = answer;
        mAnswerType = AnswerTypes.SINGLE_TRIG_NUMBER;
    }

    @Override
    public boolean isValid() {
        if (isEmpty()) {
            return true;
        }
        return mAnswer.matches(".*,k∈Z");
    }

    @Override
    public boolean equals(Answer answer) {
        if (answer.getAnswerType() != AnswerTypes.SINGLE_TRIG_NUMBER) {
            return false;
        }
        if (!isValid() || !answer.isValid()) {
            return false;
        }
        if (isEmpty() || answer.isEmpty()) {
            return isEmpty() & answer.isEmpty();
        }
        return mAnswer.equals(answer.mAnswer);
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
