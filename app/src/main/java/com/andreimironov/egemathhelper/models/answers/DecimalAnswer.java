package com.andreimironov.egemathhelper.models.answers;

public class DecimalAnswer extends Answer {
    private Double mValue;

    protected DecimalAnswer(String answer) {
        mAnswer = answer;
        mAnswerType = AnswerTypes.DECIMAL;
        if (answer.matches("\\-{0,1}[0-9]+") ||
                        answer.matches("\\-{0,1}[0-9]+,[0-9]+")) {
            mValue = Double.valueOf(answer.replace(",", "."));
        }
    }

    @Override
    public boolean isValid() {
        if (isEmpty()) {
            return true;
        }
        return mValue != null;
    }

    @Override
    public boolean equals(Answer answer) {
        if (answer.getAnswerType() != AnswerTypes.DECIMAL) {
            return false;
        }
        if (!isValid() || !answer.isValid()) {
            return false;
        }
        if (isEmpty() || answer.isEmpty()) {
            return isEmpty() & answer.isEmpty();
        }
        return Double.compare(mValue, ((DecimalAnswer) answer).mValue) == 0;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
