package com.andreimironov.egemathhelper.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.andreimironov.egemathhelper.data.Converters;
import com.andreimironov.egemathhelper.models.answers.Answer;

import java.util.Map;

public class QuestionInfo implements Parcelable {
    private Question mQuestion;
    private Map<Integer, Answer> mUserAnswers;

    public QuestionInfo(Question question, Map<Integer, Answer> userAnswers) {
        mQuestion = question;
        mUserAnswers = userAnswers;
    }

    protected QuestionInfo(Parcel in) {
        mQuestion = in.readParcelable(Question.class.getClassLoader());
        mUserAnswers = Converters.convertStringToAnswers(in.readString());
    }

    public static final Creator<QuestionInfo> CREATOR = new Creator<QuestionInfo>() {
        @Override
        public QuestionInfo createFromParcel(Parcel in) {
            return new QuestionInfo(in);
        }

        @Override
        public QuestionInfo[] newArray(int size) {
            return new QuestionInfo[size];
        }
    };

    public Question getQuestion() {
        return mQuestion;
    }

    public void setQuestion(Question question) {
        mQuestion = question;
    }

    public Map<Integer, Answer> getUserAnswers() {
        return mUserAnswers;
    }

    public void setUserAnswers(Map<Integer, Answer> userAnswers) {
        mUserAnswers = userAnswers;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mQuestion, flags);
        dest.writeString(Converters.convertAnswersToString(mUserAnswers));
    }

    public boolean isAnswerRight() {
        Map<Integer, Answer> answers = mQuestion.getAnswers();
        for (Map.Entry<Integer, Answer> entry: answers.entrySet()) {
            Integer key = entry.getKey();
            Answer answer = entry.getValue();
            Answer userAnswer = mUserAnswers.get(key);
            if (!answer.equals(userAnswer)) {
                return false;
            }
        }
        return true;
    }
}
