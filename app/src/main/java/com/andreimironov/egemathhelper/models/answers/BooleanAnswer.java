package com.andreimironov.egemathhelper.models.answers;

import android.content.Context;

import com.andreimironov.egemathhelper.R;

import androidx.annotation.NonNull;

public class BooleanAnswer extends Answer {
    protected BooleanAnswer(@NonNull String answer) {
        mAnswer = answer;
        mAnswerType = AnswerTypes.BOOLEAN;
        switch (answer) {
            case "да":
                mValue = true;
                break;
            case "нет":
                mValue = false;
                break;
            default:
                mValue = null;
                break;
        }
    }

    private Boolean mValue;

    @Override
    public boolean isValid() {
        if (isEmpty()) {
            return true;
        }
        return mValue != null;
    }

    @Override
    public boolean equals(Answer answer) {
        if (answer.getAnswerType() != AnswerTypes.BOOLEAN) {
            return false;
        }
        if (!isValid() || !answer.isValid()) {
            return false;
        }
        if (isEmpty() || answer.isEmpty()) {
            return isEmpty() && answer.isEmpty();
        }
        return mAnswer.equals(answer.mAnswer);
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
