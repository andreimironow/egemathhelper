package com.andreimironov.egemathhelper.fragments.testfragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.andreimironov.egemathhelper.R;
import com.andreimironov.egemathhelper.data.Database;
import com.andreimironov.egemathhelper.models.answers.Answer;
import com.andreimironov.egemathhelper.models.QuestionInfo;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class TestFragment extends Fragment
        implements TestAdapter.TestAdapterListener, KeyboardAdapter.KeyboardAdapterListener {
    private static final String KEY_QUESTION_INFOS = "question infos";
    private static final String TAG = "TestFragment";

    private TestFragmentListener mTestFragmentListener;
    private ArrayList<QuestionInfo> mQuestionsInfo;
    private TestLayoutManager mLayoutManager;
    private TestAdapter mTestAdapter;
    private RecyclerView mTestView;
    private LinearLayoutManager mKeyboardLayoutManager;
    private KeyboardAdapter mKeyboardAdapter;
    private RecyclerView mKeyboardView;
    private BottomSheetBehavior mKeyboardBehavior;

    private int mCoordinatorLayoutHeight;
    private int mKeyboardHeight;

    public interface TestFragmentListener {
        void onAnswerReceived(
                long id,
                Map<Integer, Answer> newUserAnswers,
                boolean areNewUserAnswersValid
        );
        void onFragmentLoaded();
    }

    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "onAttach");
        super.onAttach(context);
        mTestFragmentListener = (TestFragmentListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mQuestionsInfo = getArguments().getParcelableArrayList(KEY_QUESTION_INFOS);
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        View view = inflater.inflate(R.layout.fragment_test, container, false);

        CoordinatorLayout coordinatorLayout = view.findViewById(R.id.coordinator_layout);

        mKeyboardView = view.findViewById(R.id.keyboard);
        view.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        mCoordinatorLayoutHeight = coordinatorLayout.getHeight();
                        mKeyboardHeight = mKeyboardView.getHeight();
                    }
                }
        );
        mKeyboardLayoutManager =
                new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        mKeyboardAdapter = new KeyboardAdapter(this);
        mKeyboardView.setLayoutManager(mKeyboardLayoutManager);
        mKeyboardView.setAdapter(mKeyboardAdapter);
        mKeyboardView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {

            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                mKeyboardLayoutManager.scrollToPosition(dx > 0 ? 1 : 0);
            }
        });

        mLayoutManager = new TestLayoutManager(getActivity());
        mTestAdapter = new TestAdapter(
                mQuestionsInfo,
                this,
                getActivity()
        );
        mTestView = view.findViewById(R.id.question_recycler_view);
        mTestView.setLayoutManager(mLayoutManager);
        mTestView.setAdapter(mTestAdapter);
        mTestView.setItemViewCacheSize(Database.TEST_SIZE);

        CoordinatorLayout.LayoutParams params =
                (CoordinatorLayout.LayoutParams) mKeyboardView.getLayoutParams();
        params.setBehavior(new BottomSheetBehavior());
        mKeyboardBehavior = BottomSheetBehavior.from(mKeyboardView);
        mKeyboardBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
                ViewGroup.LayoutParams params;
                switch (newState) {
                    case BottomSheetBehavior.STATE_EXPANDED:
                        params = mTestView.getLayoutParams();
                        params.height = mCoordinatorLayoutHeight - mKeyboardHeight;
                        mTestView.requestLayout();
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        params = mTestView.getLayoutParams();
                        params.height = mCoordinatorLayoutHeight;
                        mTestView.requestLayout();
                    default:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });
        return view;
    }

    @Override
    public void setKeyboardState(int newState) {
        switch (newState) {
            case BottomSheetBehavior.STATE_COLLAPSED:
                ViewGroup.LayoutParams params = mTestView.getLayoutParams();
                params.height = mCoordinatorLayoutHeight;
                mTestView.requestLayout();
                break;
            default:
                break;
        }
        mKeyboardBehavior.setState(newState);
    }

    @Override
    public void setKeyboardListener(
            KeyboardAdapter.KeyboardListener keyboardListener,
            int answerIndex) {
        mKeyboardAdapter.setKeyboardListener(keyboardListener, answerIndex);
    }

    @Override
    public void onAnswerReceived(
            long id,
            Map<Integer, Answer> newUserAnswers,
            boolean areNewUserAnswersValid
    ) {
        mTestFragmentListener.onAnswerReceived(
                id,
                newUserAnswers,
                areNewUserAnswersValid
        );
    }

    @Override
    public void onWebViewLoaded() {
        if (mTestAdapter.areAllWebViewsLoaded()) {
            mTestFragmentListener.onFragmentLoaded();
        }
    }

    @Override
    public void onArrowClicked(int direction) {
        switch (direction) {
            case -1:
                mKeyboardLayoutManager.scrollToPosition(0);
                return;
            case 1:
                mKeyboardLayoutManager.scrollToPosition(1);
                return;
            default:
                return;
        }
    }

    public static TestFragment newInstance(ArrayList<QuestionInfo> questionInfos) {
        TestFragment fragment = new TestFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(KEY_QUESTION_INFOS, questionInfos);
        fragment.setArguments(args);
        return fragment;
    }
}