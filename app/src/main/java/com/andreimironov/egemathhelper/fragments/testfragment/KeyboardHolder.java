package com.andreimironov.egemathhelper.fragments.testfragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.andreimironov.egemathhelper.R;

import androidx.recyclerview.widget.RecyclerView;

class KeyboardHolder extends RecyclerView.ViewHolder {
    private View.OnClickListener mOnClickListener;

    public KeyboardHolder(ViewGroup parent, int viewType, View.OnClickListener listener) {
        super(getView(parent, viewType));
        mOnClickListener = listener;
        ImageButton mImageButtonRemove;
        ImageButton mImageButtonArrow;
        switch (viewType) {
            case KeyboardAdapter.VIEW_TYPE_NUMBERS:
                Button mButton0 = itemView.findViewById(R.id.button_0);
                mButton0.setOnClickListener(mOnClickListener);
                Button mButton1 = itemView.findViewById(R.id.button_1);
                mButton1.setOnClickListener(mOnClickListener);
                Button mButton2 = itemView.findViewById(R.id.button_2);
                mButton2.setOnClickListener(mOnClickListener);
                Button mButton3 = itemView.findViewById(R.id.button_3);
                mButton3.setOnClickListener(mOnClickListener);
                Button mButton4 = itemView.findViewById(R.id.button_4);
                mButton4.setOnClickListener(mOnClickListener);
                Button mButton5 = itemView.findViewById(R.id.button_5);
                mButton5.setOnClickListener(mOnClickListener);
                Button mButton6 = itemView.findViewById(R.id.button_6);
                mButton6.setOnClickListener(mOnClickListener);
                Button mButton7 = itemView.findViewById(R.id.button_7);
                mButton7.setOnClickListener(mOnClickListener);
                Button mButton8 = itemView.findViewById(R.id.button_8);
                mButton8.setOnClickListener(mOnClickListener);
                Button mButton9 = itemView.findViewById(R.id.button_9);
                mButton9.setOnClickListener(mOnClickListener);
                Button mButtonComma = itemView.findViewById(R.id.button_comma);
                mButtonComma.setOnClickListener(mOnClickListener);
                Button mButtonPlus = itemView.findViewById(R.id.button_plus);
                mButtonPlus.setOnClickListener(mOnClickListener);
                Button mButtonMinus = itemView.findViewById(R.id.button_minus);
                mButtonMinus.setOnClickListener(mOnClickListener);
                Button mButtonDivide = itemView.findViewById(R.id.button_divide);
                mButtonDivide.setOnClickListener(mOnClickListener);
                Button mButtonLeftSquareBracket = itemView.findViewById(R.id.button_left_square_bracket);
                mButtonLeftSquareBracket.setOnClickListener(mOnClickListener);
                Button mButtonRightSquareBracket = itemView.findViewById(R.id.button_right_square_bracket);
                mButtonRightSquareBracket.setOnClickListener(mOnClickListener);
                Button mButtonLeftRoundBracket = itemView.findViewById(R.id.button_left_round_bracket);
                mButtonLeftRoundBracket.setOnClickListener(mOnClickListener);
                Button mButtonRightRoundBracket = itemView.findViewById(R.id.button_right_round_bracket);
                mButtonRightRoundBracket.setOnClickListener(mOnClickListener);
                Button mButtonLeftBrace = itemView.findViewById(R.id.button_left_brace);
                mButtonLeftBrace.setOnClickListener(mOnClickListener);
                Button mButtonRightBrace = itemView.findViewById(R.id.button_right_brace);
                mButtonRightBrace.setOnClickListener(mOnClickListener);
                mImageButtonRemove = itemView.findViewById(R.id.button_numbers_remove);
                mImageButtonRemove.setOnClickListener(mOnClickListener);
                mImageButtonArrow = itemView.findViewById(R.id.button_right);
                mImageButtonArrow.setOnClickListener(mOnClickListener);
                break;
            case KeyboardAdapter.VIEW_TYPE_SYMBOLS:
                Button mButtonArcsin = itemView.findViewById(R.id.button_arcsin);
                mButtonArcsin.setOnClickListener(mOnClickListener);
                Button mButtonSquareRoot = itemView.findViewById(R.id.button_square_root);
                mButtonSquareRoot.setOnClickListener(mOnClickListener);
                Button mButtonPi = itemView.findViewById(R.id.button_pi);
                mButtonPi.setOnClickListener(mOnClickListener);
                Button mButtonK = itemView.findViewById(R.id.button_k);
                mButtonK.setOnClickListener(mOnClickListener);
                Button mButtonIn = itemView.findViewById(R.id.button_in);
                mButtonIn.setOnClickListener(mOnClickListener);
                Button mButtonZ = itemView.findViewById(R.id.button_Z);
                mButtonZ.setOnClickListener(mOnClickListener);
                Button mButtonYes = itemView.findViewById(R.id.button_yes);
                mButtonYes.setOnClickListener(mOnClickListener);
                Button mButtonNo = itemView.findViewById(R.id.button_no);
                mButtonNo.setOnClickListener(mOnClickListener);
                mImageButtonRemove = itemView.findViewById(R.id.button_symbols_remove);
                mImageButtonRemove.setOnClickListener(mOnClickListener);
                mImageButtonArrow = itemView.findViewById(R.id.button_left);
                mImageButtonArrow.setOnClickListener(mOnClickListener);
                Button mButtonSemicolon = itemView.findViewById(R.id.button_semicolon);
                mButtonSemicolon.setOnClickListener(mOnClickListener);
                Button mButtonInfinity = itemView.findViewById(R.id.button_infinity);
                mButtonInfinity.setOnClickListener(mOnClickListener);
                break;
            default:
                break;
        }
    }

    private static View getView(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case KeyboardAdapter.VIEW_TYPE_NUMBERS:
                return inflater.inflate(R.layout.keyboard_numbers, parent, false);
            case KeyboardAdapter.VIEW_TYPE_SYMBOLS:
                return inflater.inflate(R.layout.keyboard_symbols, parent, false);
            default:
                return null;
        }
    }
}
