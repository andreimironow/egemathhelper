package com.andreimironov.egemathhelper.fragments.testfragment;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.andreimironov.egemathhelper.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class KeyboardAdapter extends RecyclerView.Adapter
        implements View.OnClickListener {
    private static final int KEYBOARDS_COUNT = 2;
    public static final int VIEW_TYPE_NUMBERS = 0;
    public static final int VIEW_TYPE_SYMBOLS = 1;

    private KeyboardAdapterListener mKeyboardAdapterListener;
    private KeyboardListener mKeyboardListener;
    private int mAnswerIndex;

    public interface KeyboardListener {
        void addString(String s, int answerIndex);
        void removeString(int answerIndex);
    }

    public interface KeyboardAdapterListener {
        void onArrowClicked(int direction);
    }

    public KeyboardAdapter(KeyboardAdapterListener listener) {
        mKeyboardAdapterListener = listener;
    }

    public void setKeyboardListener(KeyboardListener keyboardListener, int answerIndex) {
        mKeyboardListener = keyboardListener;
        mAnswerIndex = answerIndex;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return VIEW_TYPE_NUMBERS;
            case 1:
                return VIEW_TYPE_SYMBOLS;
            default:
                return super.getItemViewType(position);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new KeyboardHolder(parent, viewType, this);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return KEYBOARDS_COUNT;
    }

    @Override
    public void onClick(View v) {
        if (mKeyboardListener == null) {
            return;
        }
        switch (v.getId()) {
            case R.id.button_left:
                mKeyboardAdapterListener.onArrowClicked(-1);
                break;
            case R.id.button_right:
                mKeyboardAdapterListener.onArrowClicked(1);
                break;
            case R.id.button_numbers_remove:
                mKeyboardListener.removeString(mAnswerIndex);
                break;
            case R.id.button_symbols_remove:
                mKeyboardListener.removeString(mAnswerIndex);
                break;
            default:
                Button b = (Button) v;
                String text = b.getText().toString();
                mKeyboardListener.addString(text, mAnswerIndex);
                break;
        }
    }
}

