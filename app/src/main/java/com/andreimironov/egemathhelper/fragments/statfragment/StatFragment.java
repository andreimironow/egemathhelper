package com.andreimironov.egemathhelper.fragments.statfragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andreimironov.egemathhelper.R;
import com.andreimironov.egemathhelper.models.Statistic;
import com.andreimironov.egemathhelper.models.QuestionInfo;
import com.xwray.groupie.ExpandableGroup;
import com.xwray.groupie.GroupAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class StatFragment extends Fragment {
    private static final String STATISTICS_KEY = "map key";
    private GroupAdapter mGroupAdapter;
    private RecyclerView mStatRecyclerView;
    private Map<Statistic, List<QuestionInfo>> mQuestionInfosMap;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mQuestionInfosMap = new HashMap<>();
        ArrayList<Statistic> statistics = getArguments().getParcelableArrayList(STATISTICS_KEY);
        for (Statistic statistic: statistics) {
            List<QuestionInfo> questionInfos =
                    getArguments().getParcelableArrayList(getQuestionInfosKey(statistic.getId()));
            mQuestionInfosMap.put(statistic, questionInfos);
        }
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        View view = inflater.inflate(R.layout.fragment_stat, container, false);
        mStatRecyclerView = view.findViewById(R.id.statistics_recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mStatRecyclerView.setLayoutManager(layoutManager);
        mGroupAdapter = new GroupAdapter();
        populateAdapter();
        mStatRecyclerView.setAdapter(mGroupAdapter);
        return view;
    }

    private void populateAdapter() {
        for (Map.Entry<Statistic, List<QuestionInfo>> entry: mQuestionInfosMap.entrySet()) {
            Statistic statistic = entry.getKey();
            List<QuestionInfo> questionInfos = entry.getValue();
            StatHeaderItem statHeaderItem = new StatHeaderItem(statistic, questionInfos);
            ExpandableGroup statExpGroup = new ExpandableGroup(statHeaderItem);
            for (QuestionInfo questionInfo : questionInfos) {
                QuestionInfoHeaderItem quesHeaderItem = new QuestionInfoHeaderItem(questionInfo);
                ExpandableGroup quesExpGroup = new ExpandableGroup(quesHeaderItem);
                QuestionInfoItem quesItem = new QuestionInfoItem(questionInfo);
                quesExpGroup.add(quesItem);
                statExpGroup.add(quesExpGroup);
            }
            mGroupAdapter.add(statExpGroup);
        }
    }

    public static StatFragment newInstance(Map<Statistic, List<QuestionInfo>> questionInfosMap) {
        Bundle args = new Bundle();
        ArrayList<Statistic> statistics = new ArrayList<>();
        for (Map.Entry<Statistic, List<QuestionInfo>> entry: questionInfosMap.entrySet()) {
            Statistic statistic = entry.getKey();
            List<QuestionInfo> questionInfos = entry.getValue();
            statistics.add(statistic);
            Long statId = statistic.getId();
            args.putParcelableArrayList(
                    getQuestionInfosKey(statId),
                    (ArrayList<? extends Parcelable>) questionInfos
            );
        }
        args.putParcelableArrayList(STATISTICS_KEY, statistics);
        StatFragment fragment = new StatFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private static String getQuestionInfosKey(Long statId) {
        return String.valueOf(statId);
    }
}
