package com.andreimironov.egemathhelper.fragments.statfragment;

import android.view.View;

import com.andreimironov.egemathhelper.R;
import com.andreimironov.egemathhelper.models.QuestionInfo;
import com.xwray.groupie.ExpandableGroup;
import com.xwray.groupie.ExpandableItem;
import com.xwray.groupie.Item;

import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;

public class QuestionInfoHeaderItem extends Item<QuestionInfoHeaderViewHolder> implements ExpandableItem {
    private ExpandableGroup mExpandableGroup;
    private QuestionInfo mQuestionInfo;

    public QuestionInfoHeaderItem(QuestionInfo questionInfo) {
        super();
        mQuestionInfo = questionInfo;
    }

    @Override
    public int getLayout() {
        return R.layout.stat_view_question_header;
    }


    @Override
    public void setExpandableGroup(@NonNull ExpandableGroup onToggleListener) {
        mExpandableGroup = onToggleListener;
    }

    @NonNull
    @Override
    public QuestionInfoHeaderViewHolder createViewHolder(@NonNull View itemView) {
        return new QuestionInfoHeaderViewHolder(itemView);
    }

    @Override
    public void bind(@NonNull final QuestionInfoHeaderViewHolder viewHolder, int position) {
        viewHolder.mQIndexView.setText(
                viewHolder.itemView.getContext().getString(R.string.question_index_preface) + " " +
                        mQuestionInfo.getQuestion().getIndex()
        );
        viewHolder.mIconExpand.setImageResource(getIconExpandResId());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExpandableGroup.onToggleExpanded();
                viewHolder.mIconExpand.setImageResource(getIconExpandResId());
            }
        });
        viewHolder.mIconIsRight.setImageResource(getIconIsRightResId());
    }

    @DrawableRes
    private int getIconIsRightResId() {
        if (mQuestionInfo.isAnswerRight()) {
            return R.drawable.ic_check_black_24dp;
        } else {
            return R.drawable.ic_clear_black_24dp;
        }
    }

    private int getIconExpandResId() {
        return mExpandableGroup.isExpanded()
                ? R.drawable.ic_keyboard_arrow_up_black_24dp
                : R.drawable.ic_keyboard_arrow_down_black_24dp;
    }
}
