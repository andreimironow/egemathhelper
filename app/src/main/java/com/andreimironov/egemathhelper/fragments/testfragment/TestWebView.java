package com.andreimironov.egemathhelper.fragments.testfragment;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Base64;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.andreimironov.egemathhelper.R;
import com.andreimironov.egemathhelper.models.Question;
import com.andreimironov.egemathhelper.models.QuestionInfo;
import com.andreimironov.egemathhelper.models.answers.Answer;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class TestWebView extends WebView implements KeyboardAdapter.KeyboardListener {
    private static final String INPUT_HTML_ID_PREFIX = "input_answer_";
    private static final String ANSWER_HTML_ID_PREFIX = "answer_";
    public static String MATHSCRIBE_PATH = "file:///android_asset/mathscribe/";
    private static final String JS_INTERFACE_NAME = "TestWebViewJSInterface";

    /**
     * Finds method of class TestWebView annotated with @JavascriptInterface
     * @param parameter json string
     * @return string represention of method invokation, i.e. "method(parameter)"
     */
    public static String getJSFunction(String parameter) {
        StringBuilder result = new StringBuilder();
        result.append(JS_INTERFACE_NAME + ".");
        Method[] methods = TestWebView.class.getMethods();
        for (Method method: methods) {
            if (method.getAnnotation(JavascriptInterface.class) != null) {
                result.append(method.getName());
            }
        }
        result.append("(" + parameter + ")");
        return result.toString();
    }

    public interface WebViewListener {
        void onWebViewClick(TestWebView testWebView, Map<Integer, Boolean> inputFocusInfo);
        void onWebViewLoaded(TestWebView testWebView, QuestionInfo questionInfo);
    }

    private WebViewListener mWebViewListener;
    private QuestionInfo mQuestionInfo;
    private Map<Integer, Boolean> mInputFocusInfo;

    public TestWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getSettings().setJavaScriptEnabled(true);
        setFocusable(false);
        setFocusableInTouchMode(false);
        addJavascriptInterface(this, JS_INTERFACE_NAME);
        setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mWebViewListener.onWebViewLoaded(TestWebView.this, mQuestionInfo);
            }
        });
        mInputFocusInfo = new HashMap<>();
    }

    public void setInputFocusInfo(Map<Integer, Boolean> inputFocusInfo) {
        mInputFocusInfo = inputFocusInfo;
    }

    public Map<Integer, Boolean> getInputFocusInfo() {
        return mInputFocusInfo;
    }

    public void setWebViewListener(WebViewListener webViewListener) {
        mWebViewListener = webViewListener;
    }

    @Override
    public void addString(String s, int answerIndex) {
        String inputHtmlId = getInputHtmlId(answerIndex);
        String answerHtmlId = getAnswerHtmlId(answerIndex);
        evaluateJavascript(
                "(function() { " +
                        "var start = " + inputHtmlId + ".selectionStart; " +
                        "var end = " + inputHtmlId + ".selectionEnd; " +
                        "var value = " + inputHtmlId + ".value; " +
                        "value = value.slice(0, start) + '" + s + "' + value.slice(end); " +
                        inputHtmlId + ".value = value; " +
                        inputHtmlId + ".selectionStart = start + '" + s + "'.length; " +
                        inputHtmlId + ".selectionEnd = " + inputHtmlId + ".selectionStart; " +
                        answerHtmlId + ".innerHTML = value == '' ? value : '$' + value + '$'; " +
                        "M.parseMath(document.body); " +
                        "})();",
                null
        );
    }

    @Override
    public void removeString(int answerIndex) {
        String inputHtmlId = getInputHtmlId(answerIndex);
        String answerHtmlId = getAnswerHtmlId(answerIndex);
        evaluateJavascript(
                "(function() { " +
                        "var start = " + inputHtmlId + ".selectionStart; " +
                        "var end = " + inputHtmlId + ".selectionEnd; " +
                        "if (start == end && start > 0) { start--; } " +
                        "var value = " + inputHtmlId + ".value; " +
                        "value = value.slice(0, start) + value.slice(end); " +
                        inputHtmlId + ".value = value; " +
                        inputHtmlId + ".selectionStart = start; " +
                        inputHtmlId + ".selectionEnd = start; " +
                        answerHtmlId + ".innerHTML = value == '' ? value : '$' + value + '$'; " +
                        "M.parseMath(document.body); " +
                        "})();",
                null
        );
    }

    @JavascriptInterface
    public void onWebViewClick(String jsonInputFocusInfo) {
        Map<Integer, Boolean> inputFocusInfo = getInputFocusInfo(jsonInputFocusInfo);
        mWebViewListener.onWebViewClick(this, inputFocusInfo);
    }

    public void bind(QuestionInfo questionInfo) {
        mQuestionInfo = questionInfo;
        loadDataWithBaseURL(
                "",
                getHTML(questionInfo),
                "text/html",
                "UTF-8",
                null
        );
    }

    private Map<Integer,Boolean> getInputFocusInfo(String jsonInputsInfo) {
        Map<Integer, Boolean> inputInfos = new HashMap<>();
        JsonElement jsonElement = new Gson().fromJson(jsonInputsInfo, JsonElement.class);
        JsonObject object = jsonElement.getAsJsonObject();
        Map<Integer, Answer> userAnswers = mQuestionInfo.getUserAnswers();
        for (Map.Entry<Integer, Answer> entry: userAnswers.entrySet()) {
            int answerIndex = entry.getKey();
            boolean hasFocus = object.get(String.valueOf(answerIndex)).getAsBoolean();
            inputInfos.put(answerIndex, hasFocus);
        }
        return inputInfos;
    }

    private String getAnswerHtmlId(int answerIndex) {
        return ANSWER_HTML_ID_PREFIX + answerIndex;
    }

    private String getInputHtmlId(int answerIndex) {
        return INPUT_HTML_ID_PREFIX + answerIndex;
    }

    private String getHTML(QuestionInfo questionInfo) {
        Question question = questionInfo.getQuestion();
        String titleHtml =
                "<p align='center'>" +
                        getContext().getString(R.string.question_index_preface) + " " +
                        question.getIndex() + "</p>";
        String wordingHtml = "<p align='justify'>" + question.getWording() + "</p>";
        StringBuilder imageHtml = new StringBuilder();
        if (question.getImage() != null) {
            String imageBase64 = Base64.encodeToString(question.getImage(), Base64.DEFAULT);
            imageHtml.append("<img align='center' width='100%' src='Data:image/png;base64," +
                    imageBase64 + "'/>");
        }
        StringBuilder inputListenersScript = new StringBuilder();
        StringBuilder bodyListenerScript = new StringBuilder(
                "document.body.addEventListener('click', function() { var object = {}; "
        );
        StringBuilder inputHtml = new StringBuilder("<div align='center'>");
        for (Map.Entry<Integer, Answer> entry: questionInfo.getUserAnswers().entrySet()) {
            Integer answerIndex = entry.getKey();
            Answer userAnswer = entry.getValue();
            String inputHtmlId = getInputHtmlId(answerIndex);
            String answerHtmlId = getAnswerHtmlId(answerIndex);
            inputListenersScript.append(
                    inputHtmlId + ".hasFocus = false; " +
                            inputHtmlId + ".addEventListener(" +
                            "'focus', " +
                            "function() { " + inputHtmlId + ".hasFocus = true; }, " +
                            "false); " +
                            inputHtmlId + ".addEventListener(" +
                            "'blur', " +
                            "function() { " + inputHtmlId + ".hasFocus = false; }, " +
                            "false); ");
            bodyListenerScript.append(
                    " object['" + answerIndex + "'] = " + inputHtmlId + ".hasFocus; "
            );
            String userAnswerString = userAnswer.getAnswer();
            inputHtml.append(
                    "<input id='" + inputHtmlId + "' type='text' placeholder='"+
                            getContext().getString(R.string.test_input_placeholder) + "' value='" +
                            userAnswerString + "'><p id ='" + answerHtmlId + "'></p>"
            );
        }
        inputHtml.append("</div>");
        bodyListenerScript.append(getJSFunction("JSON.stringify(object)") + "; }, false);");
        return "<html><head><link rel='stylesheet' href='" + MATHSCRIBE_PATH + "jqmath-0.4.3.css'>" +
                "<script src='" + MATHSCRIBE_PATH + "jquery-1.4.3.min.js'></script>" +
                "<script src='" + MATHSCRIBE_PATH + "jqmath-etc-0.4.6.min.js'></script>" +
                "</head>" +
                "<body>" + titleHtml + wordingHtml + imageHtml.toString() + inputHtml.toString() +
                "<script type='text/javascript'>" + inputListenersScript.toString() + "</script>" +
                "<script type='text/javascript'>" + bodyListenerScript.toString() + "</script>" +
                "</body></html>";
    }

    public void getAnswers(ValueCallback<String> callback) {
        StringBuilder script = new StringBuilder("(function() { var answers = {}; ");
        for (Map.Entry<Integer, Answer> entry: mQuestionInfo.getUserAnswers().entrySet()) {
            int answerIndex = entry.getKey();
            script.append(
                    "answers['" + answerIndex + "'] = " + getInputHtmlId(answerIndex) + ".value; "
            );
        }
        script.append("return JSON.stringify(answers); })();");
        evaluateJavascript(script.toString(), callback);
    }

    public void blurInput(int answerIndex) {
        String inputHtmlId = getInputHtmlId(answerIndex);
        evaluateJavascript(
                "(function() { " + inputHtmlId + ".blur(); " + "})();",
                result -> {
                    mInputFocusInfo.put(answerIndex, false);
                }
        );
    }

    public void blurInputs() {
        for (Map.Entry<Integer, Boolean> entry : mInputFocusInfo.entrySet()) {
            Integer answerIndex = entry.getKey();
            boolean hasFocus = entry.getValue();
            if (hasFocus) {
                blurInput(answerIndex);
            }
        }
    }
}