package com.andreimironov.egemathhelper.fragments.statfragment;

import android.view.View;

import com.andreimironov.egemathhelper.R;
import com.andreimironov.egemathhelper.models.Statistic;
import com.andreimironov.egemathhelper.models.QuestionInfo;
import com.xwray.groupie.ExpandableGroup;
import com.xwray.groupie.ExpandableItem;
import com.xwray.groupie.Item;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;

public class StatHeaderItem extends Item<StatHeaderViewHolder> implements ExpandableItem {
    private ExpandableGroup mExpandableGroup;
    private Statistic mStatistic;
    private List<QuestionInfo> mQuestionInfos;

    public StatHeaderItem(Statistic statistic, List<QuestionInfo> questionInfos) {
        super();
        mStatistic = statistic;
        mQuestionInfos = questionInfos;
    }

    @NonNull
    @Override
    public StatHeaderViewHolder createViewHolder(@NonNull View itemView) {
        return new StatHeaderViewHolder(itemView);
    }

    @Override
    public void setExpandableGroup(@NonNull ExpandableGroup onToggleListener) {
        mExpandableGroup = onToggleListener;
    }

    @Override
    public void bind(@NonNull final StatHeaderViewHolder viewHolder, int position) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yy, HH:mm:ss");
        String startTime = dateFormatter.format(new Date(mStatistic.getStartTime()));
        String endTime = dateFormatter.format(new Date(mStatistic.getEndTime()));
        viewHolder.mStartTimeView.setText(startTime);
        viewHolder.mEndTimeView.setText(endTime);
        viewHolder.mIconExpand.setImageResource(getIconResId());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExpandableGroup.onToggleExpanded();
                viewHolder.mIconExpand.setImageResource(getIconResId());
            }
        });
        int rightAnswersCount = 0;
        for (QuestionInfo questionInfo : mQuestionInfos) {
            if (questionInfo.isAnswerRight()) {
                rightAnswersCount++;
            }
        }
        viewHolder.mResultView.setText(rightAnswersCount + " из " + mQuestionInfos.size());
    }

    @Override
    public int getLayout() {
        return R.layout.stat_view_item;
    }

    private int getIconResId() {
        return mExpandableGroup.isExpanded()
                ? R.drawable.ic_keyboard_arrow_up_black_24dp
                : R.drawable.ic_keyboard_arrow_down_black_24dp;
    }
}
