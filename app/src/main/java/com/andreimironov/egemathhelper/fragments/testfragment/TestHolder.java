package com.andreimironov.egemathhelper.fragments.testfragment;

import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.andreimironov.egemathhelper.R;
import com.andreimironov.egemathhelper.models.answers.Answer;
import com.andreimironov.egemathhelper.models.QuestionInfo;
import com.andreimironov.egemathhelper.models.answers.AnswerTypes;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import androidx.recyclerview.widget.RecyclerView;

public class TestHolder extends RecyclerView.ViewHolder {
    public interface TestHolderListener {
        void onAnswerButtonClick(
                TestHolder holder,
                Map<Integer, Answer> newUserAnswers,
                boolean areNewUserAnswersValid);
    }

    private QuestionInfo mQuestionInfo;
    private TestHolderListener mTestHolderListener;
    private TestWebView mWebView;

    public TestHolder(
            View view,
            TestHolderListener testHolderListener,
            TestWebView.WebViewListener webViewListener
    ) {
        super(view);
        mTestHolderListener = testHolderListener;
        mWebView = view.findViewById(R.id.web_view);
        mWebView.setWebViewListener(webViewListener);
        Button answerButton = view.findViewById(R.id.answer_button);
        answerButton.setOnClickListener(
                v -> mWebView.getAnswers(
                    result -> {
                        Map<Integer, Answer> userAnswers = getUserAnswersFromJSON(result);
                        boolean areUserAnswersValid = true;
                        for (Map.Entry<Integer, Answer> entry: userAnswers.entrySet()) {
                            Integer index = entry.getKey();
                            Answer answer = entry.getValue();
                            if (!answer.isValid()) {
                                areUserAnswersValid = false;
                                userAnswers.put(
                                        index,
                                        Answer.emptyAnswer(answer.getAnswerType())
                                );
                            }
                        }
                        mTestHolderListener.onAnswerButtonClick(
                                this,
                                userAnswers,
                                areUserAnswersValid
                        );
                    }
                )
        );
        ImageButton menuButton = view.findViewById(R.id.image_button);
        menuButton.setOnClickListener(
                v -> new QuestionPopupMenu(v.getContext(), v, mQuestionInfo).show());
    }

    public void bind(QuestionInfo questionInfo) {
        mQuestionInfo = questionInfo;
        mWebView.bind(questionInfo);
    }

    private Map<Integer, Answer> getUserAnswersFromJSON(String jsonAnswers) {
        Gson gson = new Gson();
        Map<Integer, Answer> newUserAnswers = new HashMap<>();
        String json = gson.fromJson(jsonAnswers, String.class);
        JsonObject object = gson.fromJson(json, JsonObject.class);
        for (Map.Entry<Integer, Answer> entry: mQuestionInfo.getUserAnswers().entrySet()) {
            Integer answerIndex = entry.getKey();
            String userAnswerString = object
                    .get(String.valueOf(answerIndex))
                    .getAsString();
            AnswerTypes answerType =
                    mQuestionInfo.getQuestion().getAnswers().get(answerIndex).getAnswerType();
            Answer newUserAnswer = Answer.newInstance(
                    answerType,
                    userAnswerString
            );
            newUserAnswers.put(answerIndex, newUserAnswer);
        }
        return newUserAnswers;
    }
}