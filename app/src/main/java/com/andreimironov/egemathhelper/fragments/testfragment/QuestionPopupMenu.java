package com.andreimironov.egemathhelper.fragments.testfragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.PopupMenu;

import com.andreimironov.egemathhelper.R;
import com.andreimironov.egemathhelper.fragments.theoryfragment.TheoryUtil;
import com.andreimironov.egemathhelper.models.QuestionInfo;
import com.andreimironov.egemathhelper.models.answers.Answer;
import com.andreimironov.egemathhelper.models.answers.AnswerTypes;

import java.util.Map;

import androidx.appcompat.app.AlertDialog;

import static com.andreimironov.egemathhelper.fragments.testfragment.TestWebView.MATHSCRIBE_PATH;

public class QuestionPopupMenu extends PopupMenu implements PopupMenu.OnMenuItemClickListener {
    private QuestionInfo mQuestionInfo;
    private Context mContext;

    public QuestionPopupMenu(Context context, View anchor, QuestionInfo questionInfo) {
        super(context, anchor);
        inflate(R.menu.menu_testpopup);
        setOnMenuItemClickListener(this);
        mContext = context;
        mQuestionInfo = questionInfo;
    }

    @Override
    public boolean onMenuItemClick(final MenuItem item) {
        AlertDialog dialog = new AlertDialog.Builder(mContext)
                .create();
        View view = null;
        WebView webView = null;
        String html = null;
        switch (item.getItemId()) {
            case R.id.menu_question_popup_theory:
                view = getTheoryDialogView();
                webView = view.findViewById(R.id.theory_web_view);
                html = getTheoryHTML();
                break;
            case R.id.menu_question_popup_rules:
                view = getRulesDialogView();
                webView = view.findViewById(R.id.rules_web_view);
                html = getRulesHTML();
                break;
            default:
                break;
        }
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadDataWithBaseURL(
                "",
                html,
                "text/html",
                "UTF-8",
                null);
        dialog.setView(view);
        dialog.show();
        return true;
    }

    private View getRulesDialogView() {
        return LayoutInflater.from(mContext).inflate(R.layout.dialog_menu_popup_rules, null);
    }

    private String getRulesHTML() {
        StringBuilder body = new StringBuilder();
        Map<Integer, Answer> userAnswers = mQuestionInfo.getUserAnswers();
        int answersCount = userAnswers.size();
        for (Map.Entry<Integer, Answer> entry: userAnswers.entrySet()) {
            int index = entry.getKey();
            Answer answer = entry.getValue();
            body.append(
                    "<p align='center'>" +
                            mContext.getResources().getString(R.string.test_input_placeholder) +
                            " " +
                            (index + 1) +
                            "</p>"
            );
            body.append("<p>" + AnswerTypes.getAnswerRules(mContext, answer.getAnswerType()) + "</p>");
        }
        return "<html><head><link rel='stylesheet' href='" + MATHSCRIBE_PATH + "jqmath-0.4.3.css'>" +
                "<script src='" + MATHSCRIBE_PATH + "jquery-1.4.3.min.js'></script>" +
                "<script src='" + MATHSCRIBE_PATH + "jqmath-etc-0.4.6.min.js'></script>" +
                "</head>" +
                "<body>" + body.toString() + "</body></html>";
    }

    private View getTheoryDialogView() {
        return LayoutInflater.from(mContext).inflate(R.layout.dialog_menu_popup_theory, null);
    }

    private String getTheoryHTML() {
        StringBuilder body = new StringBuilder();
        int resId = TheoryUtil.getTheoryResId(mQuestionInfo.getQuestion().getIndex());
        body.append("<p>" + mContext.getResources().getString(resId) + "</p>");
        return "<html><head><link rel='stylesheet' href='" + MATHSCRIBE_PATH + "jqmath-0.4.3.css'>" +
                "<script src='" + MATHSCRIBE_PATH + "jquery-1.4.3.min.js'></script>" +
                "<script src='" + MATHSCRIBE_PATH + "jqmath-etc-0.4.6.min.js'></script>" +
                "</head>" +
                "<body>" + body.toString() + "</body></html>";
    }

}
