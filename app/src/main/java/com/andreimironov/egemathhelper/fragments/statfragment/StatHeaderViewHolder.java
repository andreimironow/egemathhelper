package com.andreimironov.egemathhelper.fragments.statfragment;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.andreimironov.egemathhelper.R;
import com.xwray.groupie.ViewHolder;

import androidx.annotation.NonNull;

public class StatHeaderViewHolder extends ViewHolder {
    public ImageView mIconExpand;
    public TextView mStartTimeView;
    public TextView mEndTimeView;
    public TextView mResultView;

    public StatHeaderViewHolder(@NonNull View rootView) {
        super(rootView);
        mStartTimeView = rootView.findViewById(R.id.start_time_view);
        mEndTimeView = rootView.findViewById(R.id.end_time_view);
        mIconExpand = rootView.findViewById(R.id.icon_expand);
        mResultView = rootView.findViewById(R.id.result_view);
    }

}
