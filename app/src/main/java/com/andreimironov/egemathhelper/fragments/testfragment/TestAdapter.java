package com.andreimironov.egemathhelper.fragments.testfragment;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andreimironov.egemathhelper.R;
import com.andreimironov.egemathhelper.models.answers.Answer;
import com.andreimironov.egemathhelper.models.QuestionInfo;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;

import static androidx.recyclerview.widget.RecyclerView.Adapter;

public class TestAdapter extends Adapter<TestHolder>
        implements TestHolder.TestHolderListener, TestWebView.WebViewListener {
    private List<QuestionInfo> mQuestionsInfo;
    private TestAdapterListener mTestAdapterListener;
    private Activity mActivity;
    private TestWebView mClickedWebView;
    private Map<Integer, Boolean> mLoadedQuestionsInfo;

    public interface TestAdapterListener {
        void setKeyboardState(int visibility);
        void setKeyboardListener(KeyboardAdapter.KeyboardListener keyboardListener, int answerIndex);
        void onAnswerReceived(
                long id,
                Map<Integer, Answer> newUserAnswers,
                boolean areNewUserAnswersValid
        );
        void onWebViewLoaded();
    }

    public TestAdapter(
            List<QuestionInfo> questionsInfo,
            TestAdapterListener testAdapterListener,
            Activity activity
    ) {
        mQuestionsInfo = questionsInfo;
        mTestAdapterListener = testAdapterListener;
        mActivity = activity;
        mClickedWebView = null;
        mLoadedQuestionsInfo = new HashMap<>();
        for (QuestionInfo questionInfo: mQuestionsInfo) {
            mLoadedQuestionsInfo.put(questionInfo.getQuestion().getIndex(), false);
        }
    }

    public boolean areAllWebViewsLoaded() {
        for (Map.Entry<Integer, Boolean> entry: mLoadedQuestionsInfo.entrySet()) {
            if (!entry.getValue()) {
                return false;
            }
        }
        return true;
    }

    @NonNull
    @Override
    public TestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.test_view_item, parent, false);
        return new TestHolder(view, this, this);
    }

    @Override
    public void onBindViewHolder(@NonNull TestHolder holder, int position) {
        holder.bind(mQuestionsInfo.get(position));
    }

    @Override
    public int getItemCount() {
        return mQuestionsInfo.size();
    }

    @Override
    public void onAnswerButtonClick(
            TestHolder holder,
            Map<Integer, Answer> newUserAnswers,
            boolean areNewUserAnswersValid)
    {
        mClickedWebView.blurInputs();
        mTestAdapterListener.setKeyboardState(BottomSheetBehavior.STATE_COLLAPSED);
        mTestAdapterListener.onAnswerReceived(
                mQuestionsInfo.get(holder.getAdapterPosition()).getQuestion().getId(),
                newUserAnswers,
                areNewUserAnswersValid
        );
    }

    @Override
    public void onWebViewClick(TestWebView testWebView, Map<Integer, Boolean> inputFocusInfo) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                boolean webViewHasChanged = testWebView != mClickedWebView;
                if (webViewHasChanged && mClickedWebView != null) {
                    mClickedWebView.blurInputs();
                }
                mClickedWebView = testWebView;
                mClickedWebView.setInputFocusInfo(inputFocusInfo);
                int keyboardState = BottomSheetBehavior.STATE_COLLAPSED;
                for (Map.Entry<Integer, Boolean> entry : mClickedWebView.getInputFocusInfo().entrySet()) {
                    Integer answerIndex = entry.getKey();
                    boolean hasFocus = entry.getValue();
                    if (hasFocus) {
                        mTestAdapterListener.setKeyboardListener(
                                mClickedWebView,
                                answerIndex);
                        keyboardState = BottomSheetBehavior.STATE_EXPANDED;
                        break;
                    }
                }
                mTestAdapterListener.setKeyboardState(keyboardState);
            }
        });
    }

    @Override
    public void onWebViewLoaded(TestWebView testWebView, QuestionInfo questionInfo) {
        mLoadedQuestionsInfo.put(questionInfo.getQuestion().getIndex(), true);
        mTestAdapterListener.onWebViewLoaded();
    }
}
