package com.andreimironov.egemathhelper.fragments.theoryfragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.andreimironov.egemathhelper.R;
import com.andreimironov.egemathhelper.data.Database;
import com.andreimironov.egemathhelper.fragments.rulesfragment.RulesFragment;
import com.andreimironov.egemathhelper.models.answers.AnswerTypes;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.andreimironov.egemathhelper.fragments.testfragment.TestWebView.MATHSCRIBE_PATH;

public class TheoryFragment extends Fragment {
    public static TheoryFragment newInstance() {
        TheoryFragment fragment = new TheoryFragment();
        return fragment;
    }

    private RecyclerView mRecyclerView;

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        View view = inflater.inflate(R.layout.fragment_theory, container, false);
        mRecyclerView = view.findViewById(R.id.theory_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(new TheoryFragment.TheoryAdapter(getContext()));
        mRecyclerView.setItemViewCacheSize(Database.TEST_SIZE);
        return view;
    }

    private static class TheoryAdapter extends RecyclerView.Adapter<TheoryFragment.TheoryHolder> {
        Context mContext;

        TheoryAdapter(Context context) {
            mContext = context;
        }
        @NonNull
        @Override
        public TheoryFragment.TheoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater
                    .from(mContext)
                    .inflate(R.layout.theory_view_item, parent, false);
            return new TheoryFragment.TheoryHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull TheoryFragment.TheoryHolder holder, int position) {
            holder.bind(position + 1);
        }

        @Override
        public int getItemCount() {
            return Database.TEST_SIZE;
        }
    }

    private static class TheoryHolder extends RecyclerView.ViewHolder {
        private WebView mWebView;

        public TheoryHolder(@NonNull View itemView) {
            super(itemView);
            mWebView = itemView.findViewById(R.id.web_view);
            mWebView.getSettings().setJavaScriptEnabled(true);
        }

        public void bind(int questionIndex) {
            mWebView.loadDataWithBaseURL(
                    "",
                    getHTML(questionIndex),
                    "text/html",
                    "UTF-8",
                    null
            );
        }

        private String getHTML(int questionIndex) {
            Context context = itemView.getContext();
            String titleHtml =
                    "<p align='center'>" +
                            context.getResources().getString(R.string.question_index_preface) + " " +
                            questionIndex + "</p>";
            StringBuilder body = new StringBuilder();
            int theoryResId = TheoryUtil.getTheoryResId(questionIndex);
            body.append("<p align='justify'>" + context.getResources().getString(theoryResId) + "</p>");

            return "<html><head><link rel='stylesheet' href='" + MATHSCRIBE_PATH + "jqmath-0.4.3.css'>" +
                    "<script src='" + MATHSCRIBE_PATH + "jquery-1.4.3.min.js'></script>" +
                    "<script src='" + MATHSCRIBE_PATH + "jqmath-etc-0.4.6.min.js'></script>" +
                    "</head>" +
                    "<body>" + titleHtml + body.toString() +
                    "</body></html>";
        }
    }
}
