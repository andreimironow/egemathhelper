package com.andreimironov.egemathhelper.fragments.statfragment;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.andreimironov.egemathhelper.R;
import com.xwray.groupie.ViewHolder;

import androidx.annotation.NonNull;

public class QuestionInfoHeaderViewHolder extends ViewHolder {
    public TextView mQIndexView;
    public ImageView mIconExpand;
    public ImageView mIconIsRight;

    public QuestionInfoHeaderViewHolder(@NonNull View rootView) {
        super(rootView);
        mQIndexView = rootView.findViewById(R.id.question_index_label);
        mIconExpand = rootView.findViewById(R.id.icon_expand);
        mIconIsRight = rootView.findViewById(R.id.icon_is_right);
    }
}
