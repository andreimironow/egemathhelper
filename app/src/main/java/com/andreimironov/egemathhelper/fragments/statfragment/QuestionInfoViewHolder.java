package com.andreimironov.egemathhelper.fragments.statfragment;

import android.view.View;
import android.webkit.WebView;

import com.andreimironov.egemathhelper.R;
import com.xwray.groupie.ViewHolder;

import androidx.annotation.NonNull;

public class QuestionInfoViewHolder extends ViewHolder {
    public WebView mWebView;

    public QuestionInfoViewHolder(@NonNull View rootView) {
        super(rootView);
        mWebView = rootView.findViewById(R.id.web_view);
        mWebView.getSettings().setJavaScriptEnabled(true);
    }
}