package com.andreimironov.egemathhelper.fragments.rulesfragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.andreimironov.egemathhelper.R;
import com.andreimironov.egemathhelper.models.answers.AnswerTypes;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.andreimironov.egemathhelper.fragments.testfragment.TestWebView.MATHSCRIBE_PATH;

public class RulesFragment extends Fragment {
    private RecyclerView mRecyclerView;

    public static Fragment newInstance() {
        RulesFragment fragment = new RulesFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        View view = inflater.inflate(R.layout.fragment_rules, container, false);
        mRecyclerView = view.findViewById(R.id.rules_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(new RulesAdapter(getContext()));
        mRecyclerView.setItemViewCacheSize(AnswerTypes.values().length);
        return view;
    }

    private static class RulesAdapter extends RecyclerView.Adapter<RulesHolder> {
        Context mContext;

        RulesAdapter(Context context) {
            mContext = context;
        }
        @NonNull
        @Override
        public RulesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater
                    .from(mContext)
                    .inflate(R.layout.rules_view_item, parent, false);
            return new RulesHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull RulesHolder holder, int position) {
            holder.bind(AnswerTypes.values()[position]);
        }

        @Override
        public int getItemCount() {
            return AnswerTypes.values().length;
        }
    }

    private static class RulesHolder extends RecyclerView.ViewHolder {
        private WebView mWebView;

        public RulesHolder(@NonNull View itemView) {
            super(itemView);
            mWebView = itemView.findViewById(R.id.web_view);
            mWebView.getSettings().setJavaScriptEnabled(true);
        }

        public void bind(AnswerTypes answerType) {
            mWebView.loadDataWithBaseURL(
                    "",
                    getHTML(answerType),
                    "text/html",
                    "UTF-8",
                    null
            );
        }

        private String getHTML(AnswerTypes answerType) {
            Context context = mWebView.getContext();
            StringBuilder body = new StringBuilder();
            body.append("<p align='center'>" + answerType.toString() + "</p>");
            StringBuilder builder = new StringBuilder(
                    context.getResources().getString(R.string.answer_rules_preface)
            );
            builder.append(answerType.getPossibleQuestions().toString());
            body.append("<p>" + builder.toString() + "</p>");
            body.append("<p>" + AnswerTypes.getAnswerRules(context, answerType) + "</p>");
            return "<html><head><link rel='stylesheet' href='" + MATHSCRIBE_PATH + "jqmath-0.4.3.css'>" +
                    "<script src='" + MATHSCRIBE_PATH + "jquery-1.4.3.min.js'></script>" +
                    "<script src='" + MATHSCRIBE_PATH + "jqmath-etc-0.4.6.min.js'></script>" +
                    "</head>" +
                    "<body>" + body.toString() +
                    "</body></html>";
        }
    }
}
