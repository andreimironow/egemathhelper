package com.andreimironov.egemathhelper.fragments.testfragment;

import android.content.Context;
import android.util.Log;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
/*

 */
public class TestLayoutManager extends LinearLayoutManager {
    private int mExtraLayoutSpace;

    public TestLayoutManager(Context context) {
        super(context);
        mExtraLayoutSpace = 100 * context.getResources().getConfiguration().screenHeightDp;
    }

    @Override
    protected int getExtraLayoutSpace(RecyclerView.State state) {
        return mExtraLayoutSpace;
    }
}
