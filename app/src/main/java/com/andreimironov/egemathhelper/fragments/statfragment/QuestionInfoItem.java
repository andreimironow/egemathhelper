package com.andreimironov.egemathhelper.fragments.statfragment;

import android.content.Context;
import android.util.Base64;
import android.view.View;

import com.andreimironov.egemathhelper.R;
import com.andreimironov.egemathhelper.models.answers.Answer;
import com.andreimironov.egemathhelper.models.Question;
import com.andreimironov.egemathhelper.models.QuestionInfo;
import com.xwray.groupie.Item;

import java.util.Map;

import androidx.annotation.NonNull;

import static com.andreimironov.egemathhelper.fragments.testfragment.TestWebView.MATHSCRIBE_PATH;

public class QuestionInfoItem extends Item<QuestionInfoViewHolder> {
    private QuestionInfo mQuestionInfo;

    public QuestionInfoItem(QuestionInfo questionInfo) {
        super();
        mQuestionInfo = questionInfo;
    }

    @NonNull
    @Override
    public QuestionInfoViewHolder createViewHolder(@NonNull View itemView) {
        return new QuestionInfoViewHolder(itemView);
    }

    @Override
    public void bind(@NonNull QuestionInfoViewHolder viewHolder, int position) {
        Question question = mQuestionInfo.getQuestion();
        String wordingHtml = "<p align='justify'>" + question.getWording() + "</p>";
        String imageHtml = "";
        if (question.getImage() != null) {
            String imageBase64 = Base64.encodeToString(question.getImage(), Base64.DEFAULT);
            imageHtml = "<img align='center' width='100%' src='Data:image/png;base64," +
                    imageBase64 + "'/>";
        }
        String answerHtml = "<div align='center'>";
        Context context = viewHolder.itemView.getContext();
        for (Map.Entry<Integer, Answer> entry: mQuestionInfo.getUserAnswers().entrySet()) {
            Integer answerIndex = entry.getKey();
            Answer userAnswer = entry.getValue();
            String userAnswerString = userAnswer.isEmpty()
                    ? ""
                    : "$" + userAnswer.getAnswer() + "$";
            String answerString =
                    "$" + mQuestionInfo.getQuestion().getAnswers().get(answerIndex).getAnswer() + "$";
            answerHtml +=
                    "<p>" + context.getString(R.string.stat_question_user_answer_title) + ": " +
                            userAnswerString + "</p>";
            answerHtml +=
                    "<p>" + context.getString(R.string.stat_question_right_answer_title) + ": " +
                            answerString + "</p>";
        }
        answerHtml += "</div>";
        String html =
                "<html><head><link rel='stylesheet' href='" + MATHSCRIBE_PATH + "jqmath-0.4.3.css'>" +
                        "<script src='" + MATHSCRIBE_PATH + "jquery-1.4.3.min.js'></script>" +
                        "<script src='" + MATHSCRIBE_PATH + "jqmath-etc-0.4.6.min.js'></script>" +
                        "</head>" +
                        "<body>" +
                        wordingHtml +
                        imageHtml +
                        answerHtml +
                        "</body>" +
                        "</html>"
                ;
        viewHolder.mWebView.loadDataWithBaseURL(
                "",
                html,
                "text/html",
                "UTF-8",
                null
        );
    }

    @Override
    public int getLayout() {
        return R.layout.stat_view_question;
    }
}