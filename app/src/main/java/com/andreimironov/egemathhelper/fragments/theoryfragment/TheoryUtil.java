package com.andreimironov.egemathhelper.fragments.theoryfragment;

import com.andreimironov.egemathhelper.R;

import androidx.annotation.StringRes;

public class TheoryUtil {
    @StringRes
    public static int getTheoryResId(int index) {
        switch (index) {
            case 1:
                return R.string.question_1_theory;
            case 2:
                return R.string.question_2_theory;
            case 3:
                return R.string.question_3_theory;
            case 4:
                return R.string.question_4_theory;
            case 5:
                return R.string.question_5_theory;
            case 6:
                return R.string.question_6_theory;
            case 7:
                return R.string.question_7_theory;
            case 8:
                return R.string.question_8_theory;
            case 9:
                return R.string.question_9_theory;
            case 10:
                return R.string.question_10_theory;
            case 11:
                return R.string.question_11_theory;
            case 12:
                return R.string.question_12_theory;
            case 13:
                return R.string.question_13_theory;
            case 14:
                return R.string.question_14_theory;
            case 15:
                return R.string.question_15_theory;
            case 16:
                return R.string.question_16_theory;
            case 17:
                return R.string.question_17_theory;
            case 18:
                return R.string.question_18_theory;
            case 19:
                return R.string.question_19_theory;
            default:
                return -1;
        }
    }
}
