package com.andreimironov.egemathhelper.models;

import com.andreimironov.egemathhelper.models.answers.Answer;
import com.andreimironov.egemathhelper.models.answers.AnswerTypes;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class AnswerTest {
    private void assertValidAnswersList(List<Answer> validAnswers) {
        for (Answer validAnswer: validAnswers) {
            assertThat(validAnswer.isValid(), is(true));
        }
    }

    private void assertInvalidAnswersList(List<Answer> invalidAnswers) {
        for (Answer invalidAnswer: invalidAnswers) {
            assertThat(invalidAnswer.isValid(), is(false));
        }
    }

    private void assertEqualValidAnswersList(List<Answer> equalAnswers) {
        assertValidAnswersList(equalAnswers);
        for (Answer equalAnswer: equalAnswers) {
            for (Answer equalAnswer2: equalAnswers) {
                assertThat(equalAnswer.equals(equalAnswer2), is(true));
            }
        }
    }

    private void assertInequalValidAnswersList(List<Answer> inequalAnswers) {
        assertValidAnswersList(inequalAnswers);
        for (Answer equalAnswer: inequalAnswers) {
            for (Answer equalAnswer2: inequalAnswers) {
                assertThat(equalAnswer.equals(equalAnswer2), is(equalAnswer == equalAnswer2));
            }
        }
    }

    private void assertRightValidAnswersListAndWrongValidAnswersList(
            List<Answer> rightAnswers,
            List<Answer> wrongAnswers) {
        assertValidAnswersList(rightAnswers);
        assertValidAnswersList(wrongAnswers);
        assertEqualValidAnswersList(rightAnswers);
        for (Answer rightAnswer: rightAnswers) {
            for (Answer wrongAnswer: wrongAnswers) {
                assertThat(rightAnswer.equals(wrongAnswer), is(false));
            }
        }
        for (Answer wrongAnswer: wrongAnswers) {
            for (Answer rightAnswer: rightAnswers) {
                assertThat(wrongAnswer.equals(rightAnswer), is(false));
            }
        }
    }

    @Test
    public void testDoubleAnswers() {
        List<Answer> mDoubleInvalidAnswers = new ArrayList<>();
        mDoubleInvalidAnswers.add(Answer.newInstance(AnswerTypes.DECIMAL, "1/2"));
        mDoubleInvalidAnswers.add(Answer.newInstance(AnswerTypes.DECIMAL, ",5"));
        mDoubleInvalidAnswers.add(Answer.newInstance(AnswerTypes.DECIMAL, "+0,5"));
        assertInvalidAnswersList(mDoubleInvalidAnswers);

        List<Answer> mDoubleValidAnswers = new ArrayList<>();
        mDoubleValidAnswers.add(Answer.emptyAnswer(AnswerTypes.DECIMAL));
        mDoubleValidAnswers.add(Answer.newInstance(AnswerTypes.DECIMAL, "3"));
        mDoubleValidAnswers.add(Answer.newInstance(AnswerTypes.DECIMAL, "0,5"));
        mDoubleValidAnswers.add(Answer.newInstance(AnswerTypes.DECIMAL, "-1"));
        mDoubleValidAnswers.add(Answer.newInstance(AnswerTypes.DECIMAL, "-12,345"));
        assertValidAnswersList(mDoubleValidAnswers);

        List<Answer> mDoubleEqualValidAnswers1 = new ArrayList<>();
        mDoubleEqualValidAnswers1.add(Answer.newInstance(AnswerTypes.DECIMAL, "-0,5"));
        mDoubleEqualValidAnswers1.add(Answer.newInstance(AnswerTypes.DECIMAL, "-00,5"));
        mDoubleEqualValidAnswers1.add(Answer.newInstance(AnswerTypes.DECIMAL, "-00,50"));
        assertEqualValidAnswersList(mDoubleEqualValidAnswers1);

        List<Answer> mDoubleEqualValidAnswers2 = new ArrayList<>();
        mDoubleEqualValidAnswers2.add(Answer.newInstance(AnswerTypes.DECIMAL, "-1"));
        mDoubleEqualValidAnswers2.add(Answer.newInstance(AnswerTypes.DECIMAL, "-1,0"));
        assertEqualValidAnswersList(mDoubleEqualValidAnswers2);
    }

    @Test
    public void testBooleanAnswers() {
        List<Answer> mBooleanInvalidAnswers = new ArrayList<>();
        mBooleanInvalidAnswers.add(Answer.newInstance(AnswerTypes.BOOLEAN, "0да"));
        mBooleanInvalidAnswers.add(Answer.newInstance(AnswerTypes.BOOLEAN, "не"));
        mBooleanInvalidAnswers.add(Answer.newInstance(AnswerTypes.BOOLEAN, "д"));
        assertInvalidAnswersList(mBooleanInvalidAnswers);

        List<Answer> mBooleanValidAnswers = new ArrayList<>();
        mBooleanValidAnswers.add(Answer.emptyAnswer(AnswerTypes.BOOLEAN));
        mBooleanValidAnswers.add(Answer.newInstance(AnswerTypes.BOOLEAN, "да"));
        mBooleanValidAnswers.add(Answer.newInstance(AnswerTypes.BOOLEAN, "нет"));
        assertValidAnswersList(mBooleanValidAnswers);

        List<Answer> mBooleanInequalValidAnswers = new ArrayList<>();
        mBooleanInequalValidAnswers.add(Answer.newInstance(AnswerTypes.BOOLEAN, "да"));
        mBooleanInequalValidAnswers.add(Answer.newInstance(AnswerTypes.BOOLEAN, "нет"));
        assertInequalValidAnswersList(mBooleanInequalValidAnswers);
    }

    @Test
    public void testSingleNumberAnswers() {
        List<Answer> mSingleNumberInvalidAnswers = new ArrayList<>();
        mSingleNumberInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.SINGLE_NUMBER, "-3,2"));
        mSingleNumberInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.SINGLE_NUMBER, "-3;2"));
        mSingleNumberInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.SINGLE_NUMBER, "-32;"));
        mSingleNumberInvalidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_NUMBER, "∞"));
        mSingleNumberInvalidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_NUMBER, "√3"));
        mSingleNumberInvalidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_NUMBER, "arcsin(√3/8)"));
        assertInvalidAnswersList(mSingleNumberInvalidAnswers);

        List<Answer> mSingleNumberValidAnswers = new ArrayList<>();
        mSingleNumberValidAnswers.add(Answer.emptyAnswer(AnswerTypes.SINGLE_NUMBER));
        mSingleNumberValidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_NUMBER, "+∞"));
        mSingleNumberValidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_NUMBER, "-∞"));
        mSingleNumberValidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_NUMBER, "arcsin(√{3/8})"));
        assertValidAnswersList(mSingleNumberValidAnswers);

        List<Answer> mSingleNumberEqualValidAnswers = new ArrayList<>();
        mSingleNumberEqualValidAnswers.add(
                Answer.newInstance(AnswerTypes.SINGLE_NUMBER, "√{65}-3"));
        mSingleNumberEqualValidAnswers.add(
                Answer.newInstance(AnswerTypes.SINGLE_NUMBER, "√{65}+-3"));
        mSingleNumberEqualValidAnswers.add(
                Answer.newInstance(AnswerTypes.SINGLE_NUMBER, "-3+√{65}"));
        assertEqualValidAnswersList(mSingleNumberEqualValidAnswers);
    }

    @Test
    public void testMultipleNumbersAnswers() {
        List<Answer> mMultipleNumbersInvalidAnswers = new ArrayList<>();
        mMultipleNumbersInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_NUMBERS, "2;√{6,5}+3"));
        mMultipleNumbersInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_NUMBERS, "2;√65+3"));
        mMultipleNumbersInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_NUMBERS, "3,2"));
        assertInvalidAnswersList(mMultipleNumbersInvalidAnswers);

        List<Answer> mMultipleNumbersValidAnswers = new ArrayList<>();
        mMultipleNumbersValidAnswers.add(Answer.emptyAnswer(AnswerTypes.MULTIPLE_NUMBERS));
        assertValidAnswersList(mMultipleNumbersValidAnswers);

        List<Answer> mMultipleNumbersEqualValidAnswers1 = new ArrayList<>();
        mMultipleNumbersEqualValidAnswers1.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_NUMBERS, "2;√{65}+3"));
        mMultipleNumbersEqualValidAnswers1.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_NUMBERS, "√{65}+3;2"));
        assertEqualValidAnswersList(mMultipleNumbersEqualValidAnswers1);

        List<Answer> mMultipleNumbersEqualValidAnswers2 = new ArrayList<>();
        mMultipleNumbersEqualValidAnswers1.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_NUMBERS, "-2;√{65}-3"));
        mMultipleNumbersEqualValidAnswers1.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_NUMBERS, "√{65}-3;-2"));
        mMultipleNumbersEqualValidAnswers1.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_NUMBERS, "-3+√{65};-2"));
        mMultipleNumbersEqualValidAnswers1.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_NUMBERS, "√{65}+-3;-2"));
        assertEqualValidAnswersList(mMultipleNumbersEqualValidAnswers2);

        List<Answer> mMultipleNumbersEqualValidAnswers3 = new ArrayList<>();
        mMultipleNumbersEqualValidAnswers2.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_NUMBERS, "-3π;-2π;-{11π}/6"));
        mMultipleNumbersEqualValidAnswers2.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_NUMBERS, "-{11π}/6;-3π;-2π"));
        mMultipleNumbersEqualValidAnswers2.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_NUMBERS, "-3π;-2π;-{11π}/6"));
        mMultipleNumbersEqualValidAnswers2.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_NUMBERS, "-3π;-2π;-{11π}/6;"));
        assertEqualValidAnswersList(mMultipleNumbersEqualValidAnswers3);
    }

    @Test
    public void testSingleTrigNumberAnswers() {
        List<Answer> mSingleTrigNumberInvalidAnswers = new ArrayList<>();
        mSingleTrigNumberInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.SINGLE_TRIG_NUMBER, "1"));
        mSingleTrigNumberInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.SINGLE_TRIG_NUMBER, "2πk"));
        assertInvalidAnswersList(mSingleTrigNumberInvalidAnswers);

        List<Answer> mSingleTrigNumberValidAnswers = new ArrayList<>();
        mSingleTrigNumberValidAnswers.add(
                Answer.newInstance(AnswerTypes.SINGLE_TRIG_NUMBER, "1,k∈Z"));
        mSingleTrigNumberValidAnswers.add(Answer.emptyAnswer(AnswerTypes.SINGLE_TRIG_NUMBER));
        assertValidAnswersList(mSingleTrigNumberValidAnswers);
    }

    @Test
    public void testMultipleTrigNumbersAnswers() {
        List<Answer> mMultipleTrigNumbersInvalidAnswers = new ArrayList<>();
        mMultipleTrigNumbersInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_TRIG_NUMBERS, "1;2πk,k∈Z"));
        assertInvalidAnswersList(mMultipleTrigNumbersInvalidAnswers);

        List<Answer> mMultipleTrigNumbersValidAnswers = new ArrayList<>();
        mMultipleTrigNumbersValidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_TRIG_NUMBERS, "1,k∈Z;2πk,k∈Z"));
        mMultipleTrigNumbersValidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_TRIG_NUMBERS, "1,k∈Z;2πk,k∈Z;"));
        mMultipleTrigNumbersValidAnswers.add(Answer.emptyAnswer(AnswerTypes.MULTIPLE_TRIG_NUMBERS));
        assertValidAnswersList(mMultipleTrigNumbersValidAnswers);

        List<Answer> mMultipleTrigNumbersEqualValidAnswers = new ArrayList<>();
        mMultipleTrigNumbersEqualValidAnswers.add(
                Answer.newInstance(
                        AnswerTypes.MULTIPLE_TRIG_NUMBERS,
                        "πk,k∈Z;π/6+2πk,k∈Z;{5π}/6+2πk,k∈Z"));
        mMultipleTrigNumbersEqualValidAnswers.add(
                Answer.newInstance(
                        AnswerTypes.MULTIPLE_TRIG_NUMBERS,
                        "π/6+2πk,k∈Z;πk,k∈Z;{5π}/6+2πk,k∈Z"));
        mMultipleTrigNumbersEqualValidAnswers.add(
                Answer.newInstance(
                        AnswerTypes.MULTIPLE_TRIG_NUMBERS,
                        "π/6+2πk,k∈Z;{5π}/6+2πk,k∈Z;πk,k∈Z"));
        mMultipleTrigNumbersEqualValidAnswers.add(
                Answer.newInstance(
                        AnswerTypes.MULTIPLE_TRIG_NUMBERS,
                        "πk,k∈Z;π/6+2πk,k∈Z;{5π}/6+2πk,k∈Z"));
        mMultipleTrigNumbersEqualValidAnswers.add(
                Answer.newInstance(
                        AnswerTypes.MULTIPLE_TRIG_NUMBERS,
                        "{5π}/6+2πk,k∈Z;π/6+2πk,k∈Z;πk,k∈Z"));
        mMultipleTrigNumbersEqualValidAnswers.add(
                Answer.newInstance(
                        AnswerTypes.MULTIPLE_TRIG_NUMBERS,
                        "{5π}/6+2πk,k∈Z;πk,k∈Z;π/6+2πk,k∈Z"));
        mMultipleTrigNumbersEqualValidAnswers.add(
                Answer.newInstance(
                        AnswerTypes.MULTIPLE_TRIG_NUMBERS,
                        "π/6+2πk,k∈Z;{5π}/6+2πk,k∈Zπk,k∈Z;"));
    }

    @Test
    public void testSingleSetAnswers() {
        List<Answer> mSingleSetInvalidAnswers = new ArrayList<>();
        mSingleSetInvalidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "[1,2)"));
        mSingleSetInvalidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "1;2)"));
        mSingleSetInvalidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "(1;2"));
        mSingleSetInvalidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "1;+∞)"));
        mSingleSetInvalidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "[1;+∞"));
        mSingleSetInvalidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "[1;∞)"));
        mSingleSetInvalidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "[1,+∞)"));
        mSingleSetInvalidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "(-∞,1]"));
        mSingleSetInvalidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "(-∞;1"));
        mSingleSetInvalidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "-∞;1]"));
        mSingleSetInvalidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "(∞;1]"));
        assertInvalidAnswersList(mSingleSetInvalidAnswers);

        List<Answer> mSingleSetValidAnswers = new ArrayList<>();
        mSingleSetValidAnswers.add(Answer.emptyAnswer(AnswerTypes.SINGLE_SET));
        mSingleSetValidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "(1;2)"));
        mSingleSetValidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "(1;2]"));
        mSingleSetValidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "[1;2)"));
        mSingleSetValidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "[1;2]"));
        mSingleSetValidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "(-∞;2)"));
        mSingleSetValidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "(-∞;2]"));
        mSingleSetValidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "(1;+∞)"));
        mSingleSetValidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "[1;+∞)"));
        mSingleSetValidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "(-∞;+∞)"));
        mSingleSetValidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "[-∞;+∞]"));
        mSingleSetValidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "[1;+∞]"));
        mSingleSetValidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "[-∞;1]"));
        mSingleSetValidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "√{65}-3"));
        mSingleSetValidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "√{65}+-3"));
        mSingleSetValidAnswers.add(Answer.newInstance(AnswerTypes.SINGLE_SET, "-3+√{65}"));
        assertValidAnswersList(mSingleSetValidAnswers);
    }

    @Test
    public void testMultipleSetsAnswers() {
        List<Answer> mMultipleSetsInvalidAnswers = new ArrayList<>();
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[1,2);[2;3]"));
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "1;2);[2;3]"));
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "(1;2;[2;3]"));
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "1;+∞);[2;3]"));
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[1;+∞;[2;3]"));
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[1;∞);[2;3]"));
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[1,+∞);[2;3]"));
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "(-∞,1];[2;3]"));
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "(-∞;1;[2;3]"));
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "-∞;1];[2;3]"));
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "(∞;1];[2;3]"));
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[2;3];[1,2);[2;3]"));
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[2;3];1;2);[2;3]"));
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[2;3];(1;2;[2;3]"));
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[2;3];1;+∞);[2;3]"));
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[2;3];[1;+∞;[2;3]"));
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[2;3];[1;∞);[2;3]"));
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[2;3];[1,+∞);[2;3]"));
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[2;3];(-∞,1];[2;3]"));
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[2;3];(-∞;1;[2;3]"));
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[2;3];-∞;1];[2;3]"));
        mMultipleSetsInvalidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[2;3];(∞;1];[2;3]"));
        assertInvalidAnswersList(mMultipleSetsInvalidAnswers);

        List<Answer> mMultipleSetsValidAnswers = new ArrayList<>();
        mMultipleSetsValidAnswers.add(
                Answer.emptyAnswer(AnswerTypes.MULTIPLE_SETS));
        mMultipleSetsValidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[1;2);[2;3]"));
        mMultipleSetsValidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[1;+∞);[2;3]"));
        mMultipleSetsValidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[1;+∞];[2;3]"));
        mMultipleSetsValidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[1;+∞);[2;3]"));
        mMultipleSetsValidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "(-∞;1];[2;3]"));
        mMultipleSetsValidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[-∞;1];[2;3]"));
        mMultipleSetsValidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[2;3];[1;2);[2;3]"));
        mMultipleSetsValidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[2;3];[1;+∞);[2;3]"));
        mMultipleSetsValidAnswers.add(
                Answer.newInstance(AnswerTypes.MULTIPLE_SETS, "[2;3];(-∞;1];[2;3]"));
        assertValidAnswersList(mMultipleSetsValidAnswers);

        List<Answer> mMultipleSetsEqualValidAnswers = new ArrayList<>();
        mMultipleSetsEqualValidAnswers.add(
                Answer.newInstance(
                        AnswerTypes.MULTIPLE_SETS, "(-∞;0];[√{2}-1;3];(4;+∞);√{65}+3"));
        mMultipleSetsEqualValidAnswers.add(
                Answer.newInstance(
                        AnswerTypes.MULTIPLE_SETS, "(-∞;0];(4;+∞);√{65}+3;[√{2}-1;3]"));
        mMultipleSetsEqualValidAnswers.add(
                Answer.newInstance(
                        AnswerTypes.MULTIPLE_SETS, "[√{2}-1;3];√{65}+3;(-∞;0];(4;+∞)"));
        mMultipleSetsEqualValidAnswers.add(
                Answer.newInstance(
                        AnswerTypes.MULTIPLE_SETS, "(4;+∞);(-∞;0];√{65}+3;[√{2}-1;3]"));
        mMultipleSetsEqualValidAnswers.add(
                Answer.newInstance(
                        AnswerTypes.MULTIPLE_SETS, "√{65}+3;[√{2}-1;3];(4;+∞);(-∞;0]"));
        mMultipleSetsEqualValidAnswers.add(
                Answer.newInstance(
                        AnswerTypes.MULTIPLE_SETS, "√{65}+3;(4;+∞);[√{2}-1;3];(-∞;0]"));
        mMultipleSetsEqualValidAnswers.add(
                Answer.newInstance(
                        AnswerTypes.MULTIPLE_SETS, "(-∞;0];[√{2}+-1;3];√{65}+3;(4;+∞)"));
        mMultipleSetsEqualValidAnswers.add(
                Answer.newInstance(
                        AnswerTypes.MULTIPLE_SETS, "(-∞;0];[-1+√{2};3];√{65}+3;(4;+∞)"));
        mMultipleSetsEqualValidAnswers.add(
                Answer.newInstance(
                        AnswerTypes.MULTIPLE_SETS, "(-∞;0];[-1+√{2};3];(4;+∞);√{65}+3"));
        assertEqualValidAnswersList(mMultipleSetsEqualValidAnswers);
    }
}